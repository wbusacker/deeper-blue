/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <stdio.h>

void recurse_apply_posture(uint32_t move, struct Move_posture *to_apply) {

    if (move != INVALID_MOVE_INDEX) {
        move_buffer[move].posture.white += to_apply->white;
        move_buffer[move].posture.black += to_apply->black;
        recurse_apply_posture(move_buffer[move].parent_id, to_apply);
    }
}

void import_move(uint32_t move) {
    mode = ENGINE_IMPORT;
    if (move_buffer[move].state == MOVE_UNPROPEGATED) {
        // if ((move_buffer[move].posture.white != 0) || (move_buffer[move].posture.black != 0)) {
        //     printf("About to evaluate the move with an actual posture\n");
        // }
        recurse_apply_posture(move_buffer[move].parent_id, &(move_buffer[move].posture));
        expand_tree(move);
        move_buffer[move].state = MOVE_COMPLETED;
    }
}