/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_data.c
Purpose:  CSC data definitions
*/

#include <engine.h>
#include <engine_data.h>

struct Move_data *move_buffer;
void             *buffer_start;

uint32_t estimated_used_memory_cells;

uint32_t volatile move_head;
uint32_t volatile next_move_head;

pthread_t     ui_thread_handle;
volatile bool ui_teardown;

volatile enum Engine_mode mode;
volatile bool changing_moves;