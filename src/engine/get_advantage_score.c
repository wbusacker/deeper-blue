/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>

double get_advantage_score(struct Move_posture *posture, enum Player_type_enm player_perspective) {

    double delta = (double)(posture->white) - (double)(posture->black);

    if (player_perspective == PLAYER_BLACK) {
        delta *= -1;
    }

    double divisor = ((double)(posture->white) + (double)(posture->black));

    double advantage = 0;

    if (divisor != 0){
       advantage = (delta * delta * ((delta < 0) ? -1 : 1)) / divisor;
    }

    return advantage;
}