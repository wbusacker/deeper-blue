/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: reset_move_index.c
Purpose:  Sets the move entry at the given index back to defaults
*/

#include <engine_functions.h>

void reset_move_index(uint32_t move) {

    move_buffer[move].parent_id            = INVALID_MOVE_INDEX;
    move_buffer[move].posture.black        = 0;
    move_buffer[move].posture.white        = 0;
    move_buffer[move].state                = MOVE_INVALID;
    move_buffer[move].player               = PLAYER_INVALID;
    move_buffer[move].piece_start_location = ILLEGAL_MOVE_INDEX;
    move_buffer[move].piece_end_location   = ILLEGAL_MOVE_INDEX;
    move_buffer[move].move_depth           = 2047;
}
