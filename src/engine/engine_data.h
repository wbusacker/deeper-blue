/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_data.h
Purpose:  CSC data declaration
*/

#ifndef ENGINE_DATA_H
#define ENGINE_DATA_H

#include <engine_const.h>
#include <engine_types.h>
#include <pthread.h>
#include <stdbool.h>

extern struct Move_data *move_buffer;
extern void             *buffer_start;

extern uint32_t estimated_used_memory_cells;

extern volatile uint32_t move_head;
extern volatile uint32_t next_move_head;

extern pthread_t     ui_thread_handle;
extern volatile bool ui_teardown;

extern volatile enum Engine_mode mode;

extern volatile bool changing_moves;

#endif