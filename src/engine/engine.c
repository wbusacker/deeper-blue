/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine.c
Purpose:  CSC initialization functions
*/

#include <engine_functions.h>
#include <pthread.h>
#include <stdlib.h>

uint8_t init_engine(void) {

    uint8_t return_value = 0;

    estimated_used_memory_cells = 0;

    move_head      = 0;
    next_move_head = 0;

    uint8_t *buffer_start = malloc((MOVE_BUFFER_SIZE + 1) * sizeof(struct Move_data));

    uint8_t *search_addr = buffer_start;
    while ((((uint64_t)search_addr) % MOVE_BUFFER_ALIGNMENT) != 0) {
        search_addr += 1;
    }

    move_buffer = (struct Move_data *)search_addr;

    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        reset_move_index(i);
    }

    /* Mark move index zero as complete so we can reference it as the game head */
    move_buffer[0].state      = MOVE_COMPLETED;
    move_buffer[0].player     = PLAYER_BLACK;
    move_buffer[0].move_depth = 0;

    ui_teardown = false;
    pthread_create(&ui_thread_handle, NULL, user_interface, NULL);

    /* Always expand off of the first move */
    expand_tree(0);

    mode = ENGINE_IDLE;

    changing_moves = false;

    return return_value;
}

uint8_t teardown_engine(void) {

    ui_teardown = true;

    pthread_join(ui_thread_handle, NULL);

    free(buffer_start);

    uint8_t return_value = 0;
    return return_value;
}