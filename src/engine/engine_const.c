/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_const.h>

/* Because 0 is top left in the definition but is bottom left in the board
    Rank-File to Number mapping, this will look backwards when its just
    upside down and *not* rotated
*/

enum Piece_types_enm DEFAULT_BOARD[NUM_BOARD_SPACES] =
  {WHITE_ROOK, WHITE_KNIGHT, WHITE_BISHOP, WHITE_QUEEN, WHITE_KING, WHITE_BISHOP, WHITE_KNIGHT, WHITE_ROOK,
   WHITE_PAWN, WHITE_PAWN,   WHITE_PAWN,   WHITE_PAWN,  WHITE_PAWN, WHITE_PAWN,   WHITE_PAWN,   WHITE_PAWN,
   NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,    NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,
   NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,    NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,
   NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,    NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,
   NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,    NO_PIECE,   NO_PIECE,     NO_PIECE,     NO_PIECE,
   BLACK_PAWN, BLACK_PAWN,   BLACK_PAWN,   BLACK_PAWN,  BLACK_PAWN, BLACK_PAWN,   BLACK_PAWN,   BLACK_PAWN,
   BLACK_ROOK, BLACK_KNIGHT, BLACK_BISHOP, BLACK_QUEEN, BLACK_KING, BLACK_BISHOP, BLACK_KNIGHT, BLACK_ROOK};

int8_t PAWN_RF_MOVES[NUM_PAWN_RF_MOVES][2] = {{2, -1}, {2, 0}, {2, 1}, {1, -1}, {1, 0}, {1, 1}};

int8_t ROOK_RF_MOVES[NUM_ROOK_RF_MOVES][2] =
  {{7, 0}, {6, 0}, {5, 0}, {4, 0}, {3, 0}, {2, 0}, {1, 0}, {0, 7}, {0, 6}, {0, 5}, {0, 4}, {0, 3}, {0, 2}, {0, 1}};

int8_t KNIGHT_RF_MOVES[NUM_KNIGHT_RF_MOVES][2] = {{1, 2}, {2, 1}, {1, -2}, {2, -1}};

int8_t BISHOP_RF_MOVES[NUM_BISHOP_RF_MOVES][2] = {{7, 7},
                                                  {6, 6},
                                                  {5, 5},
                                                  {4, 4},
                                                  {3, 3},
                                                  {2, 2},
                                                  {1, 1},
                                                  {7, -7},
                                                  {6, -6},
                                                  {5, -5},
                                                  {4, -4},
                                                  {3, -3},
                                                  {2, -2},
                                                  {1, -1}};

int8_t QUEEN_RF_MOVES[NUM_QUEEN_RF_MOVES][2] = {{7, 7},  {6, 6},  {5, 5},  {4, 4},  {3, 3},  {2, 2},  {1, 1},
                                                {7, -7}, {6, -6}, {5, -5}, {4, -4}, {3, -3}, {2, -2}, {1, -1},
                                                {7, 0},  {6, 0},  {5, 0},  {4, 0},  {3, 0},  {2, 0},  {1, 0},
                                                {0, 7},  {0, 6},  {0, 5},  {0, 4},  {0, 3},  {0, 2},  {0, 1}};

int8_t KING_RF_MOVES[NUM_KING_RF_MOVES][2] = {{0, -1}, {1, -1}, {1, 0}, {1, 1}};

const char *const MODE_STRINGS[NUM_ENGINE_MODES] = {"ENGINE_IDLE", "ENGINE_IMPORT", "ENGINE_EXPAND", "ENGINE_DELETE"};