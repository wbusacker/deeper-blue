/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <movement.h>
#include <stdio.h>
#include <string.h>

char *display_favored_move(void) {

    static char message_buffer[100];
    memset(message_buffer, 0, 100);

    uint32_t move = find_favored_move();

    struct Rank_file source_rf;
    position_number_to_rank_file(&source_rf, move_buffer[move].piece_start_location);

    struct Rank_file destination_rf;
    position_number_to_rank_file(&destination_rf, move_buffer[move].piece_end_location);

    /* Display this move data */
    sprintf(message_buffer,
            "%5d | %c%1d -> %c%1d | WP %10d BP %10d",
            move,
            source_rf.file + 'a',
            source_rf.rank + 1,
            destination_rf.file + 'a',
            destination_rf.rank + 1,
            move_buffer[move].posture.white,
            move_buffer[move].posture.black);

    return message_buffer;
}