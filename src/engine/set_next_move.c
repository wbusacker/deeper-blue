/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <movement.h>
#include <stdio.h>

void set_next_move(struct Rank_file *source, struct Rank_file *destination) {

    while(changing_moves);

    changing_moves = true;

    int8_t source_position      = rank_file_to_position_number(source);
    int8_t destination_position = rank_file_to_position_number(destination);

    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

        if ((move_buffer[i].parent_id == move_head) && (move_buffer[i].piece_start_location == source_position) &&
            (move_buffer[i].piece_end_location == destination_position)) {
            next_move_head = i;
            break;
        }
    }

    if (next_move_head == move_head) {
        printf("\nWarning, Unable to locate move\n");
        fflush(stdout);
    }
    // } else {
    //     printf("\nSet new move head to %d from %d\n", next_move_head, move_head);
    // }
}