/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>

void heuristic_1(enum Piece_types_enm start_board[NUM_BOARD_SPACES], enum Piece_types_enm end_board[NUM_BOARD_SPACES], struct Move_posture *posture) {

    uint16_t total_white_score = 0;
    uint16_t total_black_score = 0;

    for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
        if(start_board[i] != end_board[i]){
            switch (start_board[i]) {
                case WHITE_PAWN:
                    total_black_score += PAWN_SCORE;
                    break;
                case WHITE_ROOK:
                    total_black_score += ROOK_SCORE;
                    break;
                case WHITE_KNIGHT:
                    total_black_score += KNIGHT_SCORE;
                    break;
                case WHITE_BISHOP:
                    total_black_score += BISHOP_SCORE;
                    break;
                case WHITE_QUEEN:
                    total_black_score += QUEEN_SCORE;
                    break;
                case WHITE_KING:
                    total_black_score += KING_SCORE;
                    break;

                case BLACK_PAWN:
                    total_white_score += PAWN_SCORE;
                    break;
                case BLACK_ROOK:
                    total_white_score += ROOK_SCORE;
                    break;
                case BLACK_KNIGHT:
                    total_white_score += KNIGHT_SCORE;
                    break;
                case BLACK_BISHOP:
                    total_white_score += BISHOP_SCORE;
                    break;
                case BLACK_QUEEN:
                    total_white_score += QUEEN_SCORE;
                    break;
                case BLACK_KING:
                    total_white_score += KING_SCORE;
                    break;

                default:
                    break;
            }

        }
    }

    /* Normalize the scoring next to eachother */
    uint16_t minority = total_white_score;
    if (total_black_score < minority) {
        minority = total_black_score;
    }

    posture->white += total_white_score - minority;
    posture->black += total_black_score - minority;
}