/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_types.h
Purpose:  CSC data types
*/

#ifndef ENGINE_TYPES_H
#define ENGINE_TYPES_H

#include <engine_const.h>
#include <movement_const.h>

struct Move_posture {
    uint32_t white;
    uint32_t black;
};

struct Move_data {
    uint32_t             parent_id;
    struct Move_posture  posture;
    uint16_t             move_depth : 11;
    enum Move_state_enm  state : 3;
    enum Player_type_enm player : 2;
    int8_t               piece_start_location;
    int8_t               piece_end_location;
} __attribute__((aligned(16)));

#endif