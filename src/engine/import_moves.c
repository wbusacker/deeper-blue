/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <stdio.h>
#include <stdlib.h>
#include <timing.h>
#include <unistd.h>

void import_moves(void) {

    mode = ENGINE_IMPORT;
    // uint64_t phase_start = get_time_nanoseconds();

    /* Execute all unevaluated moves in the move buffer */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        if (move_buffer[i].state == MOVE_UNPROPEGATED) {
            import_move(i);

            /* Before we import another move, make sure that we don't exceed memory capacity */
            if (estimated_used_memory_cells >= (MOVE_BUFFER_SIZE - MOVE_PROPEGATION_COUNT)) {

                /* Get an actual read on used memory */
                estimated_used_memory_cells = num_used_cells();

                /* If we still have used too much of memory, trim down the move set */
                if (estimated_used_memory_cells >= (MOVE_BUFFER_SIZE - MOVE_PROPEGATION_COUNT)) {

                    // printf("Attempting purge by evaluation...");
                    // fflush(stdout);
                    execute_moves();
                    mode = ENGINE_IMPORT;

                    /* Get an actual read on used memory */
                    estimated_used_memory_cells = num_used_cells();

                    if (estimated_used_memory_cells >= (MOVE_BUFFER_SIZE - MOVE_PROPEGATION_COUNT)) {
                        // printf(" Attempting purge by deletion...");
                        // fflush(stdout);
                        cull_memory();
                        mode                    = ENGINE_IMPORT;
                        uint32_t new_used_count = num_used_cells();

                        if (new_used_count == estimated_used_memory_cells) {

                            printf("\nWaiting on new input\n");
                            fflush(stdout);
                        }

                        /* At this point, wait for a move application to do something */
                        while (new_used_count == estimated_used_memory_cells) {
                            apply_move();
                            mode = ENGINE_IMPORT;
                            usleep(10000);
                            new_used_count = num_used_cells();
                        }
                        estimated_used_memory_cells = new_used_count;
                    }
                }

                // printf("\n");
            }

            apply_move();
            mode = ENGINE_IMPORT;
        }
    }

    // uint64_t phase_end = get_time_nanoseconds();
    // printf("Import Moves Phase ->  %15ld ns\n", phase_end - phase_start);
}