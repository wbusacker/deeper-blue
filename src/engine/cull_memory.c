/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <float.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <timing.h>

void cull_memory(void) {
    mode = ENGINE_DELETE;
    // uint64_t phase_start = get_time_nanoseconds();

    static bool      build_array = true;
    static uint16_t *children_count;
    if (build_array) {
        children_count = (uint16_t *)malloc(MOVE_BUFFER_SIZE * sizeof(uint16_t));
        build_array    = false;
    }

    /* Clear the children count for the move set */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        children_count[i] = 0;
    }

    /* Count how many children each node has */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        uint32_t parent = move_buffer[i].parent_id;
        if (parent != INVALID_MOVE_INDEX) {
            children_count[parent]++;
        }
    }

    /* Try to find the node to delete from */
    uint32_t search_depth  = 0;
    uint32_t deletion_node = INVALID_MOVE_INDEX;

    while ((deletion_node == INVALID_MOVE_INDEX) && (moves_in_layer(search_depth) != 0)) {

        double deletion_node_posture = DBL_MAX;

        /* Search through all moves at this layer, find the least adventagous move for the other player  */
        for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

            if ((move_buffer[i].move_depth == search_depth) &&
                (get_advantage_score(&(move_buffer[i].posture), PLAYER_BLACK) < deletion_node_posture) &&
                (children_count[i] > 1)) {
                deletion_node_posture = get_advantage_score(&(move_buffer[i].posture), PLAYER_BLACK);
                deletion_node         = i;
                // printf("Potential Deletion Node %08X, has %d children\n", deletion_node, children_count[i]);
            }
        }

        if (deletion_node == INVALID_MOVE_INDEX) {
            /* No point for deletion was found, drop down to the next adversary move layer */
            search_depth += 2;
            // printf("Moving to next deletion node layer\n");
        }
    }

    if (deletion_node != INVALID_MOVE_INDEX) {

        /* Now that we know the node to delete from, find the lowest score among that */
        double   lowest_score       = DBL_MAX;
        uint32_t lowest_score_index = 0;

        for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

            if ((move_buffer[i].parent_id == deletion_node) && (move_buffer[i].state != MOVE_INVALID) &&
                (get_advantage_score(&(move_buffer[i].posture), PLAYER_WHITE) < lowest_score)) {
                lowest_score       = get_advantage_score(&(move_buffer[i].posture), PLAYER_WHITE);
                lowest_score_index = i;
                // printf("Deletion Candidate %08X\n", i);
            }
        }

        // printf("Deleting %08X\n", lowest_score_index);

        purge_move_tree(lowest_score_index);
    } else {
        /* We did not find any deletion candidates */
        printf("Unable to delete\n");
    }
    // uint64_t phase_end = get_time_nanoseconds();
    // printf("Cull Memory Phase ->   %15ld ns\n", phase_end - phase_start);
}