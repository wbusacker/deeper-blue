/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>

// uint32_t move_depth(uint32_t move) {

//     uint32_t depth = 0xFFFFFFFF;

//     if(move_buffer[move].move_depth == INVALID_MOVE_INDEX){
//         uint32_t search_id = move;

//         while ((search_id != 0) && (move_buffer[search_id].parent_id != INVALID_MOVE_INDEX)) {
//             depth++;
//             search_id = move_buffer[search_id].parent_id;
//         }

//         if (search_id != 0) {
//             /* Only way to get here is if we came off a detached head, throw a garbage result back */
//             depth = 0xFFFFFFFF;
//         }

//         move_buffer[move].move_depth = depth;
//     }

//     depth = move_buffer[move].move_depth;

//     return depth;
// }