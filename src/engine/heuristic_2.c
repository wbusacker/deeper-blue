/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <movement.h>

void heuristic_2(enum Piece_types_enm board[NUM_BOARD_SPACES], struct Move_posture *posture) {

    posture->white += (check_check(board, PLAYER_BLACK) == true) ? CHECK_SCORE : 0;
    posture->black += (check_check(board, PLAYER_WHITE) == true) ? CHECK_SCORE : 0;
}