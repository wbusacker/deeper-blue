#include <engine_functions.h>

char *move_state_enm_str(enum Move_state_enm state) {
    switch (state) {
        case MOVE_INVALID:
            return "MOVE_INVALID";
        case MOVE_UNEVALUATED:
            return "MOVE_UNEVALUATED";
        case MOVE_UNPROPEGATED:
            return "MOVE_UNPROPEGATED";
        case MOVE_COMPLETED:
            return "MOVE_COMPLETED";
        case MOVE_TO_DELETE:
            return "MOVE_TO_DELETE";
        default:
            return "UNKNOWN_STATE";
    }
}