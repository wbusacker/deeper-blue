/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <movement.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <timing.h>

uint32_t get_open_move_index(void) {
    static uint32_t new_move_index    = 0;
    bool            valid_index_found = false;
    while (! valid_index_found) {
        new_move_index++;
        new_move_index %= MOVE_BUFFER_SIZE;
        if (move_buffer[new_move_index].state == MOVE_INVALID) {
            valid_index_found = true;
            break;
        }
    }

    return new_move_index;
}

uint8_t set_rank_file_attempt(struct Move_data *move_data, int8_t rank_change, int8_t file_change) {

    /* Specifically encode this as a 1 or 0 */
    uint8_t did_move = 0;

    /* Calculate the destination RF */
    struct Rank_file destination_rf;
    position_number_to_rank_file(&destination_rf, move_data->piece_start_location);

    destination_rf.rank += rank_change;
    destination_rf.file += file_change;

    /* Only try to store this potential move if its on the board */
    if ((destination_rf.rank >= 0) && (destination_rf.rank < PLACES_IN_AXIS) && (destination_rf.file >= 0) &&
        (destination_rf.file < PLACES_IN_AXIS)) {

        did_move = 1;

        uint32_t new_index = get_open_move_index();

        /* The incoming move data isn't fully initialized so be sure to only copy needed data */
        move_buffer[new_index].parent_id            = move_data->parent_id;
        move_buffer[new_index].state                = move_data->state;
        move_buffer[new_index].player               = move_data->player;
        move_buffer[new_index].piece_start_location = move_data->piece_start_location;
        move_buffer[new_index].move_depth           = move_data->move_depth;
        move_buffer[new_index].piece_end_location   = rank_file_to_position_number(&destination_rf);
    }

    return did_move;
}

void expand_tree(uint32_t move) {
    mode = ENGINE_EXPAND;
    // uint64_t phase_start = get_time_nanoseconds();

    /* Build up the game board from the parent */
    enum Piece_types_enm board[NUM_BOARD_SPACES];

    reconstruct_history(board, move);

    enum Player_type_enm next_player = (move_buffer[move].player == PLAYER_WHITE) ? PLAYER_BLACK : PLAYER_WHITE;

    /* Iterate across the board, find all pieces owned by the next player and attempt to move them */
    for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {

        /* Figure out who owns this piece */
        enum Player_type_enm piece_owner = PLAYER_INVALID;

        switch (board[i]) {
            case WHITE_PAWN:
            case WHITE_ROOK:
            case WHITE_KNIGHT:
            case WHITE_BISHOP:
            case WHITE_QUEEN:
            case WHITE_KING:
                piece_owner = PLAYER_WHITE;
                break;

            case BLACK_PAWN:
            case BLACK_ROOK:
            case BLACK_KNIGHT:
            case BLACK_BISHOP:
            case BLACK_QUEEN:
            case BLACK_KING:
                piece_owner = PLAYER_BLACK;
                break;
            default:
                break;
        }

        if (piece_owner == next_player) {

            struct Move_data move_data = {.parent_id            = move,
                                          .state                = MOVE_UNEVALUATED,
                                          .player               = next_player,
                                          .piece_start_location = i,
                                          .move_depth           = move_buffer[move].move_depth + 1};

            uint8_t made_moves = 0;

            /* Place the piece according to the rules */
            switch (board[i]) {
                case WHITE_PAWN:
                case BLACK_PAWN:
                    for (uint8_t j = 0; j < NUM_PAWN_RF_MOVES; j++) {
                        made_moves += set_rank_file_attempt(&move_data, PAWN_RF_MOVES[j][0], PAWN_RF_MOVES[j][1]);
                        made_moves += set_rank_file_attempt(&move_data,
                                                            PAWN_RF_MOVES[j][0] * MOVE_REFLECT,
                                                            PAWN_RF_MOVES[j][1] * MOVE_REFLECT);
                    }
                    break;
                case WHITE_ROOK:
                case BLACK_ROOK:
                    for (uint8_t j = 0; j < NUM_ROOK_RF_MOVES; j++) {
                        made_moves += set_rank_file_attempt(&move_data, ROOK_RF_MOVES[j][0], ROOK_RF_MOVES[j][1]);
                        made_moves += set_rank_file_attempt(&move_data,
                                                            ROOK_RF_MOVES[j][0] * MOVE_REFLECT,
                                                            ROOK_RF_MOVES[j][1] * MOVE_REFLECT);
                    }
                    break;
                case WHITE_KNIGHT:
                case BLACK_KNIGHT:
                    for (uint8_t j = 0; j < NUM_KNIGHT_RF_MOVES; j++) {
                        made_moves += set_rank_file_attempt(&move_data, KNIGHT_RF_MOVES[j][0], KNIGHT_RF_MOVES[j][1]);
                        made_moves += set_rank_file_attempt(&move_data,
                                                            KNIGHT_RF_MOVES[j][0] * MOVE_REFLECT,
                                                            KNIGHT_RF_MOVES[j][1] * MOVE_REFLECT);
                    }
                    break;
                case WHITE_BISHOP:
                case BLACK_BISHOP:
                    for (uint8_t j = 0; j < NUM_BISHOP_RF_MOVES; j++) {
                        made_moves += set_rank_file_attempt(&move_data, BISHOP_RF_MOVES[j][0], BISHOP_RF_MOVES[j][1]);
                        made_moves += set_rank_file_attempt(&move_data,
                                                            BISHOP_RF_MOVES[j][0] * MOVE_REFLECT,
                                                            BISHOP_RF_MOVES[j][1] * MOVE_REFLECT);
                    }
                    break;
                case WHITE_QUEEN:
                case BLACK_QUEEN:
                    for (uint8_t j = 0; j < NUM_QUEEN_RF_MOVES; j++) {
                        made_moves += set_rank_file_attempt(&move_data, QUEEN_RF_MOVES[j][0], QUEEN_RF_MOVES[j][1]);
                        made_moves += set_rank_file_attempt(&move_data,
                                                            QUEEN_RF_MOVES[j][0] * MOVE_REFLECT,
                                                            QUEEN_RF_MOVES[j][1] * MOVE_REFLECT);
                    }
                    break;
                case WHITE_KING:
                case BLACK_KING:
                    for (uint8_t j = 0; j < NUM_KING_RF_MOVES; j++) {
                        made_moves += set_rank_file_attempt(&move_data, KING_RF_MOVES[j][0], KING_RF_MOVES[j][1]);
                        made_moves += set_rank_file_attempt(&move_data,
                                                            KING_RF_MOVES[j][0] * MOVE_REFLECT,
                                                            KING_RF_MOVES[j][1] * MOVE_REFLECT);
                    }
                    break;
                default:
                    break;
            }

            estimated_used_memory_cells += made_moves;
        }
    }
    // uint64_t phase_end = get_time_nanoseconds();
    // printf("Expand Tree Phase -> %15ld ns\n", phase_end - phase_start);
}