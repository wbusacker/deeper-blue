/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>

uint32_t moves_in_layer(uint32_t depth) {

    uint32_t move_count = 0;

    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        if (move_buffer[i].move_depth == depth) {
            move_count++;
        }
    }

    return move_count;
}