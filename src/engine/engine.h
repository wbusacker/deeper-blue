/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine.h
Purpose:  External CSC Header
*/

#ifndef ENGINE_H
#define ENGINE_H

#include <engine_const.h>
#include <engine_types.h>
#include <movement_types.h>
#include <stdint.h>

uint8_t init_engine(void);
uint8_t teardown_engine(void);

void display_working_move_tree(void);
void execute_moves(void);
void import_moves(void);

char *display_favored_move(void);

uint32_t num_used_cells(void);

#endif