#include <engine.h>
#include <engine_functions.h>
#include <movement.h>
#include <stdio.h>

void recurse_display_move(uint32_t move, uint8_t depth) {

    /* Tab over as far as we need to */
    for (uint8_t i = 0; i < depth; i++) {
        printf("\t");
    }

    struct Rank_file source_rf;
    position_number_to_rank_file(&source_rf, move_buffer[move].piece_start_location);

    struct Rank_file destination_rf;
    position_number_to_rank_file(&destination_rf, move_buffer[move].piece_end_location);

    /* Display this move data */
    printf("%5d | %c%1d -> %c%1d | WP %3d BP %3d | %s\n",
           move,
           source_rf.file + 'a',
           source_rf.rank + 1,
           destination_rf.file + 'a',
           destination_rf.rank + 1,
           move_buffer[move].posture.white,
           move_buffer[move].posture.black,
           move_state_enm_str(move_buffer[move].state));

    /* Display all other moves based on this one */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        if ((move_buffer[i].parent_id == move) && (move_buffer[i].state != MOVE_UNEVALUATED)) {
            recurse_display_move(i, depth + 1);
        }
    }
}

void display_working_move_tree(void) {
    recurse_display_move(0, 0);
}