/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_const.h
Purpose:  CSC constants
*/

#ifndef ENGINE_CONST_H
#define ENGINE_CONST_H

#include <movement_const.h>

// #define MOVE_BUFFER_SIZE (0x4000000)
#define MOVE_BUFFER_SIZE   (0x80000)
#define INVALID_MOVE_INDEX MOVE_BUFFER_SIZE
#define ROOT_MOVE          (0)

#define MOVE_BUFFER_ALIGNMENT (16)
#define MOVES_PER_CACHE_LINE  (64 / MOVE_BUFFER_ALIGNMENT)

#define PAWN_SCORE   (3)
#define KNIGHT_SCORE (9)
#define BISHOP_SCORE (9)
#define ROOK_SCORE   (25)
#define QUEEN_SCORE  (81)
#define KING_SCORE   (256)

#define CHECK_SCORE (256)

#define CHECKMATE_SCORE (65535)

#define MOVE_PROPEGATION_COUNT (16 * 63)

#define MOVE_REFLECT (-1)

#define NUM_PAWN_RF_MOVES   (6)
#define NUM_ROOK_RF_MOVES   (14)
#define NUM_KNIGHT_RF_MOVES (4)
#define NUM_BISHOP_RF_MOVES (14)
#define NUM_QUEEN_RF_MOVES  (28)
#define NUM_KING_RF_MOVES   (4)

enum Move_state_enm { MOVE_INVALID, MOVE_UNEVALUATED, MOVE_UNPROPEGATED, MOVE_COMPLETED, MOVE_TO_DELETE };

enum Engine_mode { ENGINE_IDLE, ENGINE_IMPORT, ENGINE_EXPAND, ENGINE_DELETE, NUM_ENGINE_MODES };

extern enum Piece_types_enm DEFAULT_BOARD[NUM_BOARD_SPACES];

extern int8_t PAWN_RF_MOVES[NUM_PAWN_RF_MOVES][2];
extern int8_t ROOK_RF_MOVES[NUM_ROOK_RF_MOVES][2];
extern int8_t KNIGHT_RF_MOVES[NUM_KNIGHT_RF_MOVES][2];
extern int8_t BISHOP_RF_MOVES[NUM_BISHOP_RF_MOVES][2];
extern int8_t QUEEN_RF_MOVES[NUM_QUEEN_RF_MOVES][2];
extern int8_t KING_RF_MOVES[NUM_KING_RF_MOVES][2];

extern const char *const MODE_STRINGS[NUM_ENGINE_MODES];

#endif