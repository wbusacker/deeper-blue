/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <stdbool.h>
#include <timing.h>
#include <worker_pool.h>

void move_processor(uint32_t id_num) {

    uint32_t scan_index       = (id_num * MOVES_PER_CACHE_LINE);
    uint8_t  cache_line_index = 0;
    while (scan_index < MOVE_BUFFER_SIZE) {

        if (move_buffer[scan_index].state == MOVE_UNEVALUATED) {
            evaluate_move(scan_index);
        }

        cache_line_index++;
        if (cache_line_index == MOVES_PER_CACHE_LINE) {
            cache_line_index = 0;
            scan_index += ((NUM_WORKERS - 1) * MOVES_PER_CACHE_LINE) + 1;
        } else {
            scan_index++;
        }
    }

    return;
}

void execute_moves(void) {
    // uint64_t phase_start = get_time_nanoseconds();

    static bool               pool_initialized = false;
    static struct Worker_pool pool;

    if (pool_initialized == false) {
        pool_initialized = true;
        init_worker_pool(&pool, move_processor);
    }

    trigger_pool(&pool);
    // uint64_t phase_end = get_time_nanoseconds();
    // printf("Execute Moves Phase -> %15ld ns\n", phase_end - phase_start);/
}