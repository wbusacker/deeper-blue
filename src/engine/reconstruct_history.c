/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: reconstruct_history.c
Purpose:  Populate the board with the history of moves that have been made
*/

#include <engine_functions.h>
#include <movement.h>
#include <string.h>

void reconstruct_history(enum Piece_types_enm board[NUM_BOARD_SPACES], uint32_t move) {

    /* If we're at the root of the game, set the board to initial conditions */
    if (move == ROOT_MOVE) {
        memcpy(board, DEFAULT_BOARD, NUM_BOARD_SPACES * sizeof(enum Piece_types_enm));
    } else {
        /* Expand out the parent move */
        reconstruct_history(board, move_buffer[move].parent_id);

        /* Apply this move action */
        board[move_buffer[move].piece_end_location]   = board[move_buffer[move].piece_start_location];
        board[move_buffer[move].piece_start_location] = NO_PIECE;

        /* If the move made was a pawn reaching the other side, always promote to queen */
        for (uint8_t file = 0; file < PLACES_IN_RANK; file++) {
            struct Rank_file rf = {.rank = 7, .file = file};
            if (board[rank_file_to_position_number(&rf)] == WHITE_PAWN) {
                board[rank_file_to_position_number(&rf)] = WHITE_QUEEN;
            }
        }
    }
}