/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <stdbool.h>
#include <stdio.h>
#include <worker_pool.h>

static volatile uint32_t move_to_purge;

void scan_for_purge(uint32_t id_num) {

    /* Iterate all of the moves that aren't invalid and check their ancestry
        to see if they trace up to the move we're removing
    */

    uint32_t scan_index       = (id_num * MOVES_PER_CACHE_LINE);
    uint8_t  cache_line_index = 0;
    while (scan_index < MOVE_BUFFER_SIZE) {

        uint32_t parent_id = move_buffer[scan_index].parent_id;
        while (parent_id != INVALID_MOVE_INDEX) {
            /* If we've found either the move we're look to delete, or a move that has
                already been marked for removal (because it is a child) mark this move
                for deletion
            */
            if ((parent_id == move_to_purge) || (move_buffer[parent_id].state == MOVE_TO_DELETE)) {
                move_buffer[scan_index].state = MOVE_TO_DELETE;
                parent_id                     = INVALID_MOVE_INDEX;
            } else {
                parent_id = move_buffer[parent_id].parent_id;
            }
        }

        cache_line_index++;
        if (cache_line_index == MOVES_PER_CACHE_LINE) {
            cache_line_index = 0;
            scan_index += ((NUM_WORKERS - 1) * MOVES_PER_CACHE_LINE) + 1;
        } else {
            scan_index++;
        }
    }
}

void purge_move_tree(uint32_t move) {
    mode = ENGINE_DELETE;

    static bool               pool_initialized = false;
    static struct Worker_pool pool;

    if (pool_initialized == false) {
        pool_initialized = true;
        init_worker_pool(&pool, scan_for_purge);
    }

    move_to_purge = move;

    trigger_pool(&pool);

    /* Always mark the move itself as to delete */
    move_buffer[move].state = MOVE_TO_DELETE;

    /* Now that we've marked all of the to deletes, actually delete them */
    uint32_t used_memory = 0;
    for (uint32_t scan_index = 0; scan_index < MOVE_BUFFER_SIZE; scan_index++) {
        if (move_buffer[scan_index].state == MOVE_TO_DELETE) {
            reset_move_index(scan_index);
        } else if (move_buffer[scan_index].state != MOVE_INVALID) {
            used_memory++;
        }
    }

    // printf(" %20d used cells\n", used_memory);

    return;
}