/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <movement.h>

void evaluate_move(uint32_t move) {

    /* Build up the game board from the parent */
    enum Piece_types_enm start_board[NUM_BOARD_SPACES];

    reconstruct_history(start_board, move_buffer[move].parent_id);

    /* Check to see if we can make the move */
    enum Move_response_enm move_response =
      evaluate_move_attempt(start_board, move_buffer[move].piece_start_location, move_buffer[move].piece_end_location);

    if (move_response == LEGAL_MOVE) {

        enum Piece_types_enm end_board[NUM_BOARD_SPACES];

        for(uint8_t i = 0; i < NUM_BOARD_SPACES; i++){
            end_board[i] = start_board[i];
        }

        move_buffer[move].state                       = MOVE_UNPROPEGATED;
        end_board[move_buffer[move].piece_end_location]   = start_board[move_buffer[move].piece_start_location];
        end_board[move_buffer[move].piece_start_location] = NO_PIECE;

        /* Apply the heuristic on this new board */
        heuristic_1(start_board, end_board, &(move_buffer[move].posture));
        heuristic_2(end_board, &(move_buffer[move].posture));

        /* Check for game ending condition. If either King is lost, award that player with a ton of points
            and set the move state to completed since it does not need to be propegated
        */
        bool white_king_alive = false;
        bool black_king_alive = false;
        for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
            if (end_board[i] == WHITE_KING) {
                white_king_alive = true;
            }
            if (end_board[i] == BLACK_KING) {
                black_king_alive = true;
            }
        }

        if (white_king_alive == false) {
            move_buffer[move].posture.black += CHECKMATE_SCORE;
            move_buffer[move].state = MOVE_COMPLETED;
        }

        if (black_king_alive == false) {
            move_buffer[move].posture.white += CHECKMATE_SCORE;
            move_buffer[move].state = MOVE_COMPLETED;
        }

    } else {
        reset_move_index(move);
    }
}