/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

void *user_interface(void *arg __attribute__((unused))) {

    fcntl(fileno(stdin), F_SETFL, fcntl(fileno(stdin), F_GETFL) | O_NONBLOCK);

    struct termios original_settings;
    struct termios noncanonical_settings;

    tcgetattr(fileno(stdin), &original_settings);

    noncanonical_settings = original_settings;

    noncanonical_settings.c_lflag &= ~(ICANON | ECHO);
    tcsetattr(fileno(stdin), TCSANOW, &noncanonical_settings);

    char command_buffer[10];
    memset(command_buffer, 0, 10);
    uint8_t seen_chars = 0;

    while (ui_teardown == false) {

        char new_char = 0;
        int  num_read = read(fileno(stdin), &new_char, 1);

        if (num_read == 1) {

            command_buffer[seen_chars] = new_char;
            if (new_char == 0x7F) {
                if (seen_chars != 0) {
                    seen_chars--;
                }
                command_buffer[seen_chars] = '\0';
            } else {
                seen_chars++;
            }

            if (new_char == '\n') {

                struct Rank_file source      = {.rank = command_buffer[1] - '1', .file = command_buffer[0] - 'a'};
                struct Rank_file destination = {.rank = command_buffer[3] - '1', .file = command_buffer[2] - 'a'};

                set_next_move(&source, &destination);

                seen_chars = 0;
                memset(command_buffer, 0, 10);
            }
        }

        /* Calculate how many moves ahead the engine has searched */
        uint16_t current_search_depth = 0;
        for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
            if ((move_buffer[i].move_depth > current_search_depth) && (move_buffer[i].move_depth != 2047)) {
                current_search_depth = move_buffer[i].move_depth;
            }
        }
        current_search_depth -= move_buffer[move_head].move_depth;

        /* Calculate how many moves are left under move head */
        uint16_t possible_moves = 0;
        for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
            if ((move_buffer[i].parent_id == move_head)) {
                possible_moves++;
            }
        }

        printf("\r%-50s %15s %10.6f%% %5d %5d Ready New Move %5s %-10s",
               display_favored_move(),
               MODE_STRINGS[mode],
               (num_used_cells() * 100.0) / ((double)(MOVE_BUFFER_SIZE)),
               current_search_depth,
               possible_moves,
               changing_moves ? "False" : "True",
               command_buffer);
        fflush(stdout);
        usleep(10000);
    }

    tcsetattr(fileno(stdin), TCSANOW, &original_settings);
    fcntl(fileno(stdin), F_SETFL, fcntl(fileno(stdin), F_GETFL) & ~O_NONBLOCK);

    return NULL;
}