/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>

uint32_t num_used_cells(void) {

    uint32_t used_cells = 0;
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
        if (move_buffer[i].state != MOVE_INVALID) {
            used_cells++;
        }
    }

    return used_cells;
}