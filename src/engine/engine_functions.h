/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_functions.h
Purpose:  CSC local function declarations
*/

#ifndef ENGINE_FUNCTIONS_H
#define ENGINE_FUNCTIONS_H

#include <engine.h>
#include <engine_data.h>
#include <movement_const.h>

void reset_move_index(uint32_t move);
void reset_move(uint32_t move);

void reconstruct_history(enum Piece_types_enm board[NUM_BOARD_SPACES], uint32_t move);

void expand_tree(uint32_t move);
void evaluate_move(uint32_t move);

void import_move(uint32_t move);

void process_single_turn();

void heuristic_1(enum Piece_types_enm start_board[NUM_BOARD_SPACES], enum Piece_types_enm end_board[NUM_BOARD_SPACES], struct Move_posture *posture);
void heuristic_2(enum Piece_types_enm board[NUM_BOARD_SPACES], struct Move_posture *posture);

char *move_state_enm_str(enum Move_state_enm state);

void purge_move_tree(uint32_t move);
void cull_memory(void);

double get_advantage_score(struct Move_posture *posture, enum Player_type_enm player_perspective);

uint32_t find_favored_move(void);
// uint32_t move_depth(uint32_t move);
uint32_t moves_in_layer(uint32_t depth);

void apply_move(void);

void *user_interface(void *arg);
void  set_next_move(struct Rank_file *source, struct Rank_file *destination);

#endif