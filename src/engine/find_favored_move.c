/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>

uint32_t find_favored_move(void) {

    double   best_score = -2;
    uint32_t best_index = 0;

    // uint32_t advantage_index = 0;
    // int32_t advantage_score = -0x80000000;

    // uint32_t white_index = 0;
    // int32_t white_score = -0x80000000;

    // uint32_t black_index = 0;
    // int32_t black_score = -0x80000000;

    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

        if (move_buffer[i].parent_id == move_head) {

            int32_t advantage = get_advantage_score(&(move_buffer[i].posture), PLAYER_WHITE);

            if (advantage > best_score) {
                best_score = advantage;
                best_index = i;
            }

            // if(move_buffer[i].posture.white > white_score){
            //     white_score = move_buffer[i].posture.white;
            //     white_index = i;
            // }

            // if(move_buffer[i].posture.black > black_score){
            //     black_score = move_buffer[i].posture.black;
            //     black_index = i;
            // }
        }
    }

    // best_index = advantage_index;
    // /* Whichever move has the lower black score is who we go with */
    // if(move_buffer[advantage_index].posture.black > move_buffer[white_index].posture.black){
    //     best_index = white_index;
    // }

    return best_index;
}