/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: engine_template.c
Purpose:
*/

#include <engine_functions.h>
#include <stdio.h>

void apply_move(void) {

    if (next_move_head != move_head) {

        if(move_buffer[next_move_head].parent_id == move_head){
            /* Delete all moves for the next layer except for what is now the new move head */
            for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {
                if ((move_buffer[i].parent_id == move_head) && (i != next_move_head)) {
                    purge_move_tree(i);
                }
            }
            move_head = next_move_head;
        } else {
            /* Reset state */
            next_move_head = move_head;
        }



    }

    changing_moves = false;
}