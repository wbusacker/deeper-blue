/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: evaluate_move_attempt.c
Purpose:  Check to see if the requested move is a valid move
*/

#include <movement_functions.h>
#include <stdlib.h>
#include <string.h>

enum Move_response_enm
evaluate_move_attempt(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination) {

    movement_function evaluation = NULL;

    /* Initially assume the player is black, then let the switch fall throughs correct when necessary */
    enum Player_type_enm player = PLAYER_BLACK;

    switch (board[source]) {
        case WHITE_PAWN:
            player = PLAYER_WHITE;
            __attribute__((fallthrough));
        case BLACK_PAWN:
            evaluation = evaluate_pawn_move;
            break;
        case WHITE_ROOK:
            player = PLAYER_WHITE;
            __attribute__((fallthrough));
        case BLACK_ROOK:
            evaluation = evaluate_rook_move;
            break;
        case WHITE_KNIGHT:
            player = PLAYER_WHITE;
            __attribute__((fallthrough));
        case BLACK_KNIGHT:
            evaluation = evaluate_knight_move;
            break;
        case WHITE_BISHOP:
            player = PLAYER_WHITE;
            __attribute__((fallthrough));
        case BLACK_BISHOP:
            evaluation = evaluate_bishop_move;
            break;
        case WHITE_QUEEN:
            player = PLAYER_WHITE;
            __attribute__((fallthrough));
        case BLACK_QUEEN:
            evaluation = evaluate_queen_move;
            break;
        case WHITE_KING:
            player = PLAYER_WHITE;
            __attribute__((fallthrough));
        case BLACK_KING:
            evaluation = evaluate_king_move;
            break;
        default:
            break;
    }

    /* Check to see if this move would result in a check condition occuring */
    enum Move_response_enm valid_move = evaluation(board, source, destination);

    if (valid_move == LEGAL_MOVE) {

        enum Piece_types_enm new_board[NUM_BOARD_SPACES];
        memcpy(new_board, board, NUM_BOARD_SPACES * sizeof(enum Piece_types_enm));

        new_board[destination] = board[source];
        new_board[source]      = NO_PIECE;

        if (check_check(new_board, player)) {
            valid_move = ILLEGAL_MOVE;
        }
    }

    return valid_move;
}