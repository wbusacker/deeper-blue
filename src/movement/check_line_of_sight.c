/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: evaluate_king_move.c
Purpose:  Check to see if the requested move is a valid move for a king
*/

#include <engine_const.h>
#include <movement_functions.h>
#include <stdlib.h>
#include <string.h>

uint8_t
check_line_of_sight(enum Piece_types_enm board[NUM_BOARD_SPACES], enum Player_type_enm player, uint8_t location) {

    uint8_t num_pieces_white = 0;
    uint8_t num_pieces_black = 0;

    /* For search purposes, buffer the board and remove the piece at the location we're looking at */
    enum Piece_types_enm search_board[NUM_BOARD_SPACES];
    memcpy(search_board, board, sizeof(enum Piece_types_enm) * NUM_BOARD_SPACES);

    struct Rank_file location_rf;
    position_number_to_rank_file(&location_rf, location);

    /* Scan along the different axis, searching for a different player piece
        then checking if that piece can currently move onto the king
    */

    /* Check horizontal piece movements first */
    for (uint8_t search_axis = 0; search_axis < 2; search_axis++) {
        for (uint8_t axis_position = 0; axis_position < PLACES_IN_AXIS; axis_position++) {

            struct Rank_file search_rf = {
              .file = (search_axis == 0) ? axis_position : location_rf.file,
              .rank = (search_axis == 0) ? location_rf.rank : axis_position,
            };

            int8_t search_position = rank_file_to_position_number(&search_rf);

            switch (search_board[search_position]) {
                case WHITE_ROOK:
                    num_pieces_white +=
                      (evaluate_rook_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                case WHITE_QUEEN:
                    num_pieces_white +=
                      (evaluate_queen_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                case BLACK_ROOK:
                    num_pieces_black +=
                      (evaluate_rook_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                case BLACK_QUEEN:
                    num_pieces_black +=
                      (evaluate_queen_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                    break;
                default:
                    break;
            }
        }
    }

    /* Then check diagonal movements */
    for (uint8_t search_axis = 0; search_axis < 2; search_axis++) {

        struct Rank_file lower_rf = location_rf;

        if (lower_rf.rank < lower_rf.file) {
            lower_rf.rank -= lower_rf.rank;
            lower_rf.file -= lower_rf.rank;
        } else {
            lower_rf.rank -= lower_rf.file;
            lower_rf.file -= lower_rf.file;
        }

        for (uint8_t axis_position = 0; axis_position < PLACES_IN_AXIS; axis_position++) {

            struct Rank_file search_rf = {
              .file = lower_rf.file + axis_position,
              .rank = lower_rf.rank + axis_position,
            };

            int8_t search_position = rank_file_to_position_number(&search_rf);

            if (search_position < NUM_BOARD_SPACES) {

                switch (search_board[search_position]) {
                    case WHITE_BISHOP:
                        num_pieces_white +=
                          (evaluate_bishop_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                        break;
                    case WHITE_QUEEN:
                        num_pieces_white +=
                          (evaluate_queen_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                        break;
                    case BLACK_BISHOP:
                        num_pieces_black +=
                          (evaluate_bishop_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                        break;
                    case BLACK_QUEEN:
                        num_pieces_black +=
                          (evaluate_queen_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /* Individually handle the pawn conditions */

    for (uint8_t i = 0; i < 4; i++) {
        struct Rank_file search_rf = location_rf;

        switch (i) {
            case 0:
                search_rf.rank += 1;
                search_rf.file += 1;
                break;
            case 1:
                search_rf.rank -= 1;
                search_rf.file += 1;
                break;
            case 2:
                search_rf.rank += 1;
                search_rf.file -= 1;
                break;
            case 3:
                search_rf.rank -= 1;
                search_rf.file -= 1;
                break;
        }

        int8_t search_position = rank_file_to_position_number(&search_rf);

        /* Pawns are special in that they can only capture if the destination piece is of the other
            color and something is there. So always suddenly put a piece there to capture and then take
            it away after the check is complete
        */
        switch (search_board[search_position]) {
            case WHITE_PAWN:
                search_board[location] = BLACK_PAWN;
                num_pieces_white += (evaluate_pawn_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                search_board[location] = NO_PIECE;
                break;
            case BLACK_PAWN:
                search_board[location] = WHITE_PAWN;
                num_pieces_black += (evaluate_pawn_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                search_board[location] = NO_PIECE;
                break;
            default:
                break;
        }
    }

    /* Individually handle the knight conditions */
    for (uint8_t inflection = 0; inflection < 2; inflection++) {
        for (uint8_t i = 0; i < NUM_KNIGHT_RF_MOVES; i++) {
            struct Rank_file search_rf = location_rf;

            search_rf.rank += KNIGHT_RF_MOVES[i][0] * ((inflection == 0) ? 1 : -1);
            search_rf.file += KNIGHT_RF_MOVES[i][0] * ((inflection == 0) ? 1 : -1);

            int8_t search_position = rank_file_to_position_number(&search_rf);

            switch (search_board[search_position]) {
                case WHITE_KNIGHT:
                    num_pieces_white +=
                      (evaluate_knight_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                case BLACK_KNIGHT:
                    num_pieces_white +=
                      (evaluate_knight_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                default:
                    break;
            }
        }
    }

    /* Lastly, check by King */
    for (uint8_t inflection = 0; inflection < 2; inflection++) {
        for (uint8_t i = 0; i < NUM_KING_RF_MOVES; i++) {
            struct Rank_file search_rf = location_rf;

            search_rf.rank += KING_RF_MOVES[i][0] * ((inflection == 0) ? 1 : -1);
            search_rf.file += KING_RF_MOVES[i][0] * ((inflection == 0) ? 1 : -1);

            int8_t search_position = rank_file_to_position_number(&search_rf);

            switch (search_board[search_position]) {
                case WHITE_KING:
                    num_pieces_white +=
                      (evaluate_king_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                case BLACK_KING:
                    num_pieces_white +=
                      (evaluate_king_move(search_board, search_position, location) == LEGAL_MOVE) ? 1 : 0;
                    break;
                default:
                    break;
            }
        }
    }

    return (player == PLAYER_WHITE) ? num_pieces_white : num_pieces_black;
}