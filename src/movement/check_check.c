/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: evaluate_king_move.c
Purpose:  Check to see if the requested move is a valid move for a king
*/

#include <engine_const.h>
#include <movement_functions.h>
#include <stdlib.h>

bool check_check(enum Piece_types_enm board[NUM_BOARD_SPACES], enum Player_type_enm player) {

    bool player_in_check = false;

    /* First, find the location of the king, then calculate its rank file */
    struct Rank_file king_rf;
    uint8_t          king_location = 0;
    for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
        if ((player == PLAYER_WHITE ? WHITE_KING : BLACK_KING) == board[i]) {
            king_location = i;
            position_number_to_rank_file(&king_rf, i);
            break;
        }
    }

    player_in_check =
      (check_line_of_sight(board, (player == PLAYER_WHITE) ? PLAYER_BLACK : PLAYER_WHITE, king_location) != 0);

    return player_in_check;
}