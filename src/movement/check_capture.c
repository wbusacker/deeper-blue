/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: check_capture.c
Purpose:  See if the destination piece is of the opposite player
*/

#include <movement_functions.h>

bool check_capture(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination) {

    enum Player_type_enm moving_player   = PLAYER_INVALID;
    enum Player_type_enm opponent_player = PLAYER_INVALID;

    /* Figure out what player we are */
    switch (board[source]) {
        case WHITE_PAWN:
        case WHITE_ROOK:
        case WHITE_KNIGHT:
        case WHITE_BISHOP:
        case WHITE_QUEEN:
        case WHITE_KING:
            moving_player = PLAYER_WHITE;
            break;
        case BLACK_PAWN:
        case BLACK_ROOK:
        case BLACK_KNIGHT:
        case BLACK_BISHOP:
        case BLACK_QUEEN:
        case BLACK_KING:
            moving_player = PLAYER_BLACK;
            break;
        default:
            break;
    }

    /* Figure out the opponent, if he exists */
    switch (board[destination]) {
        case WHITE_PAWN:
        case WHITE_ROOK:
        case WHITE_KNIGHT:
        case WHITE_BISHOP:
        case WHITE_QUEEN:
        case WHITE_KING:
            opponent_player = PLAYER_WHITE;
            break;
        case BLACK_PAWN:
        case BLACK_ROOK:
        case BLACK_KNIGHT:
        case BLACK_BISHOP:
        case BLACK_QUEEN:
        case BLACK_KING:
            opponent_player = PLAYER_BLACK;
            break;
        default:
            break;
    }

    return ((moving_player != opponent_player) && (moving_player != PLAYER_INVALID) &&
            (opponent_player != PLAYER_INVALID));
}