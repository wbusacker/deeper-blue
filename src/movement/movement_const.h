/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: movement_const.h
Purpose:  CSC constants
*/

#ifndef MOVEMENT_CONST_H
#define MOVEMENT_CONST_H

#include <stdint.h>

#define NUM_BOARD_SPACES (64)

#define BLACK_STARTING_RANK (8)
#define WHITE_STARTING_RANK (0)

#define PLACES_IN_RANK (8)
#define PLACES_IN_AXIS (8)

#define MOVE_PLUS_RANK  (8)
#define MOVE_PLUS_FILE  (1)
#define MOVE_PLUS_RIGHT (9)
#define MOVE_PLUS_LEFT  (7)

#define MOVE_FORWARD (1)
#define MOVE_REVERSE (-1)

#define KNIGHT_LONG_LEG_DELTA  (2)
#define KNIGHT_SHORT_LEG_DELTA (1)

#define WHITE_PAWN_STARTING_RANK (1)
#define BLACK_PAWN_STARTING_RANK (6)

#define ILLEGAL_MOVE_INDEX NUM_BOARD_SPACES

enum Piece_types_enm {
    WHITE_PAWN,
    WHITE_ROOK,
    WHITE_KNIGHT,
    WHITE_BISHOP,
    WHITE_QUEEN,
    WHITE_KING,
    BLACK_PAWN,
    BLACK_ROOK,
    BLACK_KNIGHT,
    BLACK_BISHOP,
    BLACK_QUEEN,
    BLACK_KING,
    NO_PIECE,
    NUM_PIECE_TYPES
};

enum Player_type_enm { PLAYER_WHITE, PLAYER_BLACK, PLAYER_COUNT, PLAYER_INVALID };

enum Move_response_enm { ILLEGAL_MOVE, LEGAL_MOVE, PAWN_PROMOTION };

#endif