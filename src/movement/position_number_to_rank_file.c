/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: position_number_to_rank_file.c
Purpose:  Convert the given number and find its rank and file counterparts
*/

#include <movement_functions.h>

void position_number_to_rank_file(struct Rank_file *rf, int8_t position) {

    if ((position < NUM_BOARD_SPACES) & (position >= 0)) {
        rf->rank = (position / PLACES_IN_AXIS);
        rf->file = (position % PLACES_IN_AXIS);
    } else {
        rf->rank = 0x77;
        rf->file = 0x77;
    }
}