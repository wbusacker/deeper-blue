/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: movement_functions.h
Purpose:  CSC local function declarations
*/

#ifndef MOVEMENT_FUNCTIONS_H
#define MOVEMENT_FUNCTIONS_H

#include <movement.h>
#include <movement_data.h>
#include <stdbool.h>

enum Move_response_enm
evaluate_pawn_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
enum Move_response_enm
evaluate_rook_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
enum Move_response_enm
evaluate_king_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
enum Move_response_enm
evaluate_queen_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
enum Move_response_enm
evaluate_knight_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
enum Move_response_enm
evaluate_bishop_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);

bool check_collision(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
bool check_capture(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);
uint8_t
check_line_of_sight(enum Piece_types_enm board[NUM_BOARD_SPACES], enum Player_type_enm player, uint8_t location);

enum Player_type_enm get_piece_player(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t position);

#endif