/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: movement_data.c
Purpose:  CSC data definitions
*/

#include <movement.h>
#include <movement_data.h>
