/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: check_collision.h
Purpose:  Check to see if there are any pieces in the direction of travel requested
            but we do *not* check to see if the destination has a piece in its location
*/

#include <movement_functions.h>
#include <stdlib.h>

bool check_collision(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination) {

    bool collision_detected = false;

    /* Calculate what direction we're traveling in */

    int8_t move_direction = 0;

    struct Rank_file source_rf;
    position_number_to_rank_file(&source_rf, source);

    struct Rank_file destination_rf;
    position_number_to_rank_file(&destination_rf, destination);

    int8_t delta = abs(destination - source);

    /* Horizontal movement */
    if (source_rf.rank == destination_rf.rank) {
        move_direction = MOVE_PLUS_FILE;

        /* Vertical Movement */
    } else if (source_rf.file == destination_rf.file) {
        move_direction = MOVE_PLUS_RANK;

        /* Angle Right */
    } else if ((delta % MOVE_PLUS_RIGHT) == 0) {
        move_direction = MOVE_PLUS_RIGHT;

        /* Angle Left */
    } else if ((delta % MOVE_PLUS_LEFT) == 0) {
        move_direction = MOVE_PLUS_LEFT;
    }

    /* Only start iterating if we know what direction we're going */
    if (move_direction != 0) {

        /* Are we moving backwards? */
        if (destination < source) {
            move_direction *= MOVE_REVERSE;
        }

        /* Start scanning to the next direction */
        int8_t scan_index = source + move_direction;

        while (scan_index != destination) {
            /* If we find anything along the way, latch that */
            if (board[scan_index] != NO_PIECE) {
                collision_detected = true;
            }

            scan_index += move_direction;
        }
    }

    return collision_detected;
}