/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: evaluate_bishop_move.c
Purpose:  Check to see if the requested move is a valid move for a bishop
*/

#include <movement_functions.h>
#include <stdlib.h>

enum Move_response_enm
evaluate_bishop_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination) {

    enum Move_response_enm move_evaluation = ILLEGAL_MOVE;

    struct Rank_file source_rf;
    position_number_to_rank_file(&source_rf, source);

    struct Rank_file destination_rf;
    position_number_to_rank_file(&destination_rf, destination);

    /* Ensure that the requested movement is on the axis that the queen can move */
    if (abs(source_rf.rank - destination_rf.rank) == abs(source_rf.file - destination_rf.file)) {

        if (check_collision(board, source, destination) == false) {
            /* No intermediate collision detected, check to see if we can step on the destination */

            if (board[destination] == NO_PIECE) {
                move_evaluation = LEGAL_MOVE;
            } else {
                move_evaluation = check_capture(board, source, destination) ? LEGAL_MOVE : ILLEGAL_MOVE;
            }
        }
    }

    return move_evaluation;
}