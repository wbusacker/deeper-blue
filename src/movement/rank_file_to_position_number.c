/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: position_number_to_rank_file.c
Purpose:  Convert the given number and find its rank and file counterparts
*/

#include <movement_functions.h>

uint8_t rank_file_to_position_number(struct Rank_file *rf) {

    uint8_t position_number = 0x77;

    if ((rf->rank >= 0) && (rf->rank < PLACES_IN_AXIS) && (rf->file >= 0) && (rf->file < PLACES_IN_AXIS)) {
        position_number = (rf->rank * PLACES_IN_AXIS) + rf->file;
    }

    return position_number;
}