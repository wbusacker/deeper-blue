/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: evaluate_pawn_move.c
Purpose:  Check to see if the requested move is a valid move for a pawn
*/

#include <movement_functions.h>
#include <stdlib.h>

enum Move_response_enm
evaluate_pawn_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination) {

    enum Move_response_enm move_evaluation = ILLEGAL_MOVE;

    /* Figure out which direction these pawns are meant to be traveling */
    int8_t               direction_multiplier = 0;
    enum Player_type_enm player               = PLAYER_INVALID;

    switch (board[source]) {
        case WHITE_PAWN:
            direction_multiplier = MOVE_FORWARD;
            player               = PLAYER_WHITE;
            break;
        case BLACK_PAWN:
            direction_multiplier = MOVE_REVERSE;
            player               = PLAYER_BLACK;
            break;
        default:
            break;
    }

    /* Standard move ahead one check */
    if (destination == (source + (direction_multiplier * MOVE_PLUS_RANK))) {

        /* The space ahead "must" be open */
        move_evaluation = (board[destination] == NO_PIECE) ? LEGAL_MOVE : ILLEGAL_MOVE;

    } else if (destination == (source + (direction_multiplier * MOVE_PLUS_RANK * 2))) {

        /* Few different conditions must be met,
            Pawn must be on its starting rank
            Nothing is in the square directly ahead
            Nothing is in the square two ahead
        */

        struct Rank_file pawn_rf;
        position_number_to_rank_file(&pawn_rf, source);

        move_evaluation =
          ((((player == PLAYER_WHITE) ? WHITE_PAWN_STARTING_RANK : BLACK_PAWN_STARTING_RANK) == pawn_rf.rank) &&
           (NO_PIECE == board[source + (direction_multiplier * MOVE_PLUS_RANK)]) &&
           (NO_PIECE == board[source + (direction_multiplier * MOVE_PLUS_RANK * 2)]))
            ? LEGAL_MOVE
            : ILLEGAL_MOVE;

    } else if ((destination == (source + (direction_multiplier * MOVE_PLUS_RIGHT))) ||
               (destination == (source + (direction_multiplier * MOVE_PLUS_LEFT)))) {

        /* Check to see if we could capture here */
        move_evaluation = check_capture(board, source, destination) ? LEGAL_MOVE : ILLEGAL_MOVE;
    }
    return move_evaluation;
}