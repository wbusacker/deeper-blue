/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: movement_types.h
Purpose:  CSC data types
*/

#ifndef MOVEMENT_TYPES_H
#define MOVEMENT_TYPES_H

#include <movement_const.h>

typedef enum Move_response_enm (*movement_function)(enum Piece_types_enm[NUM_BOARD_SPACES], int8_t, int8_t);

struct Rank_file {
    int8_t rank;
    int8_t file;
};

#endif