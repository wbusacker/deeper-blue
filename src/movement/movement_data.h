/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: movement_data.h
Purpose:  CSC data declaration
*/

#ifndef MOVEMENT_DATA_H
#define MOVEMENT_DATA_H

#include <movement_const.h>
#include <movement_types.h>

#endif