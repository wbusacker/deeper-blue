/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: evaluate_king_move.c
Purpose:  Check to see if the requested move is a valid move for a king
*/

#include <movement_functions.h>
#include <stdlib.h>

enum Move_response_enm
evaluate_king_move(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination) {

    enum Move_response_enm move_evaluation = ILLEGAL_MOVE;

    struct Rank_file source_rf;
    position_number_to_rank_file(&source_rf, source);

    struct Rank_file destination_rf;
    position_number_to_rank_file(&destination_rf, destination);

    struct Rank_file move_delta = {.rank = abs(destination_rf.rank - source_rf.rank),
                                   .file = abs(destination_rf.file - source_rf.file)};

    if ((move_delta.rank <= 1) && (move_delta.file <= 1)) {
        if (board[destination] == NO_PIECE) {
            move_evaluation = LEGAL_MOVE;
        } else {
            move_evaluation = check_capture(board, source, destination) ? LEGAL_MOVE : ILLEGAL_MOVE;
        }
    }

    return move_evaluation;
}