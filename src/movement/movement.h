/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: movement.h
Purpose:  External CSC Header
*/

#ifndef MOVEMENT_H
#define MOVEMENT_H

#include <movement_const.h>
#include <movement_types.h>
#include <stdbool.h>
#include <stdint.h>

/*

    Standard Chess Board layout + position ID numbers

    8 | 56  57  58  59  60  61  62  63
      |
    7 | 48  49  50  51  52  53  54  55
      |
    6 | 40  41  42  43  44  45  46  47
      |
 R  5 | 32  33  34  35  36  37  38  39
 A    |
 N  4 | 24  25  26  27  28  29  30  31
 K    |
    3 | 16  17  18  19  20  21  22  23
      |
    2 |  8   9  10  11  12  13  14  15
      |
    1 |  0   1   2   3   4   5   6   7
      ----------------------------------
         a   b   c   d   e   f   g   h
                     FILE

    White is always assumed to be on the bottom


    Horizontal / File Movement <------->

                               ^
    Vertical / Rank Movement   |
                               \/

                      /
    Diagonal Right   /
                    /

                    \
    Diagonal Left    \
                      \
*/

uint8_t init_movement(void);
uint8_t teardown_movement(void);

enum Move_response_enm
evaluate_move_attempt(enum Piece_types_enm board[NUM_BOARD_SPACES], int8_t source, int8_t destination);

uint8_t rank_file_to_position_number(struct Rank_file *rf);
void    position_number_to_rank_file(struct Rank_file *rf, int8_t position);

bool check_check(enum Piece_types_enm board[NUM_BOARD_SPACES], enum Player_type_enm player);

#endif