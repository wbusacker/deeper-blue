#include <engine.h>
#include <pthread.h>
#include <stdio.h>

int main(void) {

    init_engine();

    for (uint16_t turns = 0; turns < 1000; turns++) {

        execute_moves();

        import_moves();

        // printf("Round %d complete, %10d cells used, %5.1f%% utilization\t",
        //        turns,
        //        num_used_cells(),
        //        (100.0 * (double)(num_used_cells())) / MOVE_BUFFER_SIZE);
        // display_favored_move();
    }

    // display_working_move_tree();
    printf("%d\n", num_used_cells());

    return 0;
}
