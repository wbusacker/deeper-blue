/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_const.h
Purpose:  CSC constants
*/

#ifndef WORKER_POOL_CONST_H
#define WORKER_POOL_CONST_H

#define NUM_WORKERS (12)

enum Worker_status_enm { WORKER_DISABLE, WORKER_GO, WORKER_SLEEP, WORKER_ACTIVE, WORKER_COMPLETE, WORKER_EXIT };

#endif