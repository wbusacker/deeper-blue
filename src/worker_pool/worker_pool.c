/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool.c
Purpose:  CSC initialization functions
*/

#include <stdlib.h>
#include <worker_pool_functions.h>

void init_worker_pool(struct Worker_pool *pool, worker_function target_function) {

    pool->worker_list = (struct Worker *)malloc(sizeof(struct Worker) * NUM_WORKERS);
    pool->num_workers = NUM_WORKERS;

    for (uint8_t i = 0; i < NUM_WORKERS; i++) {
        pool->worker_list[i].handle    = 0;
        pool->worker_list[i].worker_id = i;
        pool->worker_list[i].status    = WORKER_DISABLE;
        pool->worker_list[i].function  = target_function;

        pthread_mutex_init(&(pool->worker_list[i].lock), NULL);
        pthread_mutex_lock(&(pool->worker_list[i].lock));

        pthread_create(&(pool->worker_list[i].handle), NULL, common_thread_control, &(pool->worker_list[i]));
    }
}

void teardown_worker_pool(struct Worker_pool *pool) {

    for (uint8_t i = 0; i < NUM_WORKERS; i++) {
        pool->worker_list[i].status = WORKER_EXIT;
        pthread_join(pool->worker_list[i].handle, NULL);
    }

    free(pool->worker_list);
}