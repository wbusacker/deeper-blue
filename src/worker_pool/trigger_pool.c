/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_template.c
Purpose:
*/

#include <unistd.h>
#include <worker_pool_functions.h>

void trigger_pool(struct Worker_pool *pool) {

    /* Let all of the threads go */
    for (uint8_t i = 0; i < NUM_WORKERS; i++) {
        pool->worker_list[i].status = WORKER_GO;
        pthread_mutex_unlock(&(pool->worker_list[i].lock));
    }

    /* Wait for all of the threads to go active.*/
    for (uint8_t i = 0; i < NUM_WORKERS; i++) {

        while (pool->worker_list[i].status != WORKER_ACTIVE) {
            usleep(100000);
        }
    }

    /* Now that all of the threads are busy, attempt to lock them all */
    for (uint8_t i = 0; i < NUM_WORKERS; i++) {
        pool->worker_list[i].status = WORKER_DISABLE;
        pthread_mutex_lock(&(pool->worker_list[i].lock));
    }
}