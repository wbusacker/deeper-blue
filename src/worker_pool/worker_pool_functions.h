/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_functions.h
Purpose:  CSC local function declarations
*/

#ifndef WORKER_POOL_FUNCTIONS_H
#define WORKER_POOL_FUNCTIONS_H

#include <worker_pool.h>
#include <worker_pool_data.h>

void *common_thread_control(void *arg);

#endif