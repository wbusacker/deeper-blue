/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_types.h
Purpose:  CSC data types
*/

#ifndef WORKER_POOL_TYPES_H
#define WORKER_POOL_TYPES_H

#include <pthread.h>
#include <worker_pool_const.h>

typedef void (*worker_function)(uint32_t);

struct Worker {
    pthread_t                       handle;
    uint32_t                        worker_id;
    volatile enum Worker_status_enm status;
    worker_function                 function;
    pthread_mutex_t                 lock;
};

struct Worker_pool {
    struct Worker *worker_list;
    uint8_t        num_workers;
};

#endif