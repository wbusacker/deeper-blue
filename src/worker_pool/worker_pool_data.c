/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_data.c
Purpose:  CSC data definitions
*/

#include <worker_pool.h>
#include <worker_pool_data.h>
