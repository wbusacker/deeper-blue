/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool.h
Purpose:  External CSC Header
*/

#ifndef WORKER_POOL_H
#define WORKER_POOL_H

#include <stdint.h>
#include <worker_pool_const.h>
#include <worker_pool_types.h>

void init_worker_pool(struct Worker_pool *pool, worker_function target_function);
void teardown_worker_pool(struct Worker_pool *pool);

void trigger_pool(struct Worker_pool *pool);

#endif