/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_data.h
Purpose:  CSC data declaration
*/

#ifndef WORKER_POOL_DATA_H
#define WORKER_POOL_DATA_H

#include <worker_pool_const.h>
#include <worker_pool_types.h>

#endif