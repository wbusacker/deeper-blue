/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: worker_pool_template.c
Purpose:
*/

#include <unistd.h>
#include <worker_pool_functions.h>

void *common_thread_control(void *arg) {

    struct Worker *worker = (struct Worker *)arg;

    while (worker->status != WORKER_EXIT) {

        pthread_mutex_lock(&(worker->lock));

        /* Tell the controller we're active */
        worker->status = WORKER_ACTIVE;
        worker->function(worker->worker_id);

        /* Wait for the controller to acknowledge that we executed */
        pthread_mutex_unlock(&(worker->lock));
        while (worker->status == WORKER_ACTIVE) {
            usleep(10000);
        }
    }

    return NULL;
}