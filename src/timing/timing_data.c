/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing_data.c
Purpose:  CSC data definitions
*/

#include <timing.h>
#include <timing_data.h>
