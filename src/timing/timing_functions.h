/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing_functions.h
Purpose:  CSC local function declarations
*/

#ifndef TIMING_FUNCTIONS_H
#define TIMING_FUNCTIONS_H

#include <timing.h>
#include <timing_data.h>

#endif