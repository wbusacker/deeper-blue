/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing_const.h
Purpose:  CSC constants
*/

#ifndef TIMING_CONST_H
#define TIMING_CONST_H

#define NS_PER_S (1E9)

#endif