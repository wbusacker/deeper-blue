/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing_data.h
Purpose:  CSC data declaration
*/

#ifndef TIMING_DATA_H
#define TIMING_DATA_H

#include <timing_const.h>
#include <timing_types.h>

#endif