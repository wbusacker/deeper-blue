/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing.h
Purpose:  External CSC Header
*/

#ifndef TIMING_H
#define TIMING_H

#include <stdint.h>
#include <timing_const.h>
#include <timing_types.h>

uint8_t init_timing(void);
uint8_t teardown_timing(void);

uint64_t get_time_nanoseconds();

#endif