/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing_types.h
Purpose:  CSC data types
*/

#ifndef TIMING_TYPES_H
#define TIMING_TYPES_H

#include <stdint.h>
#include <timing_const.h>

struct Timing_data {
    uint64_t total_elapsed_time_ns;
    uint64_t total_calls;
    uint64_t average_time_ns;
};

#endif