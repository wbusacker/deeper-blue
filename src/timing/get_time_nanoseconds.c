/*
Copyright (c) 2022 Will Busacker
See project license for more details

Project:  deeper_blue
Filename: timing_template.c
Purpose:
*/

#include <time.h>
#include <timing_functions.h>

uint64_t get_time_nanoseconds(void) {

    struct timespec t;

    clock_gettime(CLOCK_MONOTONIC, &t);

    return (t.tv_sec * NS_PER_S) + t.tv_nsec;
}