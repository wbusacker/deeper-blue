#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <engine.h>
#include <engine_data.h>
}

TEST(Engine_initialization, move_buffer_alignment) {
    init_engine();
    ASSERT_EQ((uint64_t)(&(move_buffer[0])) % 16, 0);
    teardown_engine();
}
