#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <engine_types.h>
}

TEST(Engine_types_constraints, sizeof_move_struct) {
    ASSERT_LE(sizeof(struct Move_data), 16);
    printf("Move Structure size %ld\n", sizeof(struct Move_data));
}
