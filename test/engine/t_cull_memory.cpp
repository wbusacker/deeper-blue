#include <array>
#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <engine_functions.h>
#include <movement_const.h>
}

void set_move_buffer(uint32_t             id,
                     uint32_t             parent_id,
                     uint16_t             posture_white,
                     uint16_t             posture_black,
                     uint8_t              move_depth,
                     enum Move_state_enm  state,
                     enum Player_type_enm player) {
    move_buffer[id].parent_id            = parent_id;
    move_buffer[id].posture.white        = posture_white;
    move_buffer[id].posture.black        = posture_black;
    move_buffer[id].state                = state;
    move_buffer[id].player               = player;
    move_buffer[id].piece_start_location = 0;
    move_buffer[id].piece_end_location   = 0;
    move_buffer[id].move_depth           = move_depth;
}

const uint32_t END_SET_DATA = 24;

class T_cull_memory : public ::testing::TestWithParam<std::tuple<int, std::array<enum Move_state_enm, END_SET_DATA>>> {
    public:
    T_cull_memory() { }

    void SetUp() {
        init_engine();

        /* Set all buffer entries to point to the last as their parent
            except for the very first one */
        for (uint32_t i = 1; i < MOVE_BUFFER_SIZE; i++) {
            set_move_buffer(i, MOVE_BUFFER_SIZE - 1, 0x77, 0xAA, 255, MOVE_INVALID, PLAYER_WHITE);
        }
        /* To make sure we don't have circular references, have the last entry parent be nothing */
        set_move_buffer(MOVE_BUFFER_SIZE - 1, INVALID_MOVE_INDEX, 0x77, 0xAA, 255, MOVE_INVALID, PLAYER_WHITE);

        /* Construct the test case linkages */
        set_move_buffer(1, 0, 0, 10, 1, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(2, 0, 10, 0, 1, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(3, 0, 26, 12, 1, MOVE_COMPLETED, PLAYER_WHITE);

        set_move_buffer(4, 2, 0, 0, 2, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(5, 2, 0, 0, 2, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(6, 2, 0, 0, 2, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(7, 3, 0, 0, 2, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(8, 3, 10, 0, 2, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(9, 3, 16, 12, 2, MOVE_COMPLETED, PLAYER_BLACK);

        set_move_buffer(10, 8, 1, 1, 3, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(11, 8, 1, 0, 3, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(12, 8, 7, 0, 3, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(13, 9, 0, 10, 3, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(14, 9, 6, 0, 3, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(15, 9, 10, 2, 3, MOVE_COMPLETED, PLAYER_WHITE);

        set_move_buffer(16, 15, 1, 0, 4, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(17, 15, 3, 0, 4, MOVE_COMPLETED, PLAYER_BLACK);
        set_move_buffer(18, 15, 6, 2, 4, MOVE_COMPLETED, PLAYER_BLACK);

        set_move_buffer(19, 18, 1, 0, 5, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(20, 18, 2, 1, 5, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(21, 18, 3, 1, 5, MOVE_COMPLETED, PLAYER_WHITE);

        set_move_buffer(22, 21, 1, 0, 6, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(23, 21, 2, 1, 6, MOVE_COMPLETED, PLAYER_WHITE);
        set_move_buffer(24, 21, 3, 1, 6, MOVE_COMPLETED, PLAYER_WHITE);

        num_cull_passes = std::get<0>(GetParam());
        check_values    = std::get<1>(GetParam());
    }

    void TearDown() {

        /* Always make sure the rest of the moves don't have their state altered */
        /* Put in garbage values in the rest of the buffers */
        for (uint32_t i = END_SET_DATA + 1; i < MOVE_BUFFER_SIZE; i++) {
            EXPECT_EQ(move_buffer[i].parent_id,
                      (i == (MOVE_BUFFER_SIZE - 1)) ? INVALID_MOVE_INDEX : (MOVE_BUFFER_SIZE - 1));
            EXPECT_EQ(move_buffer[i].posture.white, 0x77);
            EXPECT_EQ(move_buffer[i].posture.black, 0xAA);
            EXPECT_EQ(move_buffer[i].state, MOVE_INVALID);
            EXPECT_EQ(move_buffer[i].player, PLAYER_WHITE);
            EXPECT_EQ(move_buffer[i].piece_end_location, 0);
            EXPECT_EQ(move_buffer[i].piece_start_location, 0);
        }

        teardown_engine();
    }

    int                                           num_cull_passes;
    std::array<enum Move_state_enm, END_SET_DATA> check_values;
};

TEST_P(T_cull_memory, Execute_cull_passes) {

    for (uint8_t i = 0; i < num_cull_passes; i++) {
        cull_memory();
    }

    // for (uint8_t i = 0; i < END_SET_DATA; i++) {
    //     printf("%d", i);
    //     ASSERT_EQ(move_buffer[i + 1].state, check_values[i]);
    // }

    EXPECT_EQ(move_buffer[1].state, check_values[0]);
    EXPECT_EQ(move_buffer[2].state, check_values[1]);
    EXPECT_EQ(move_buffer[3].state, check_values[2]);
    EXPECT_EQ(move_buffer[4].state, check_values[3]);
    EXPECT_EQ(move_buffer[5].state, check_values[4]);
    EXPECT_EQ(move_buffer[6].state, check_values[5]);
    EXPECT_EQ(move_buffer[7].state, check_values[6]);
    EXPECT_EQ(move_buffer[8].state, check_values[7]);
    EXPECT_EQ(move_buffer[9].state, check_values[8]);
    EXPECT_EQ(move_buffer[10].state, check_values[9]);
    EXPECT_EQ(move_buffer[11].state, check_values[10]);
    EXPECT_EQ(move_buffer[12].state, check_values[11]);
    EXPECT_EQ(move_buffer[13].state, check_values[12]);
    EXPECT_EQ(move_buffer[14].state, check_values[13]);
    EXPECT_EQ(move_buffer[15].state, check_values[14]);
    EXPECT_EQ(move_buffer[16].state, check_values[15]);
    EXPECT_EQ(move_buffer[17].state, check_values[16]);
    EXPECT_EQ(move_buffer[18].state, check_values[17]);
    EXPECT_EQ(move_buffer[19].state, check_values[18]);
    EXPECT_EQ(move_buffer[20].state, check_values[19]);
    EXPECT_EQ(move_buffer[21].state, check_values[20]);
    EXPECT_EQ(move_buffer[22].state, check_values[21]);
    EXPECT_EQ(move_buffer[23].state, check_values[22]);
    EXPECT_EQ(move_buffer[24].state, check_values[23]);
}

// clang-format off
INSTANTIATE_TEST_SUITE_P(Cull_memory,
                         T_cull_memory,
                         ::testing::Values(
                             /*                                                               1               2               3               4               5               6               7               8               9               10              11              12              13              14              15              16              17              18              19              20              21              22              23              24              */
                             std::make_tuple(0,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(1,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(2,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(3,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(4,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(5,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(6,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(7,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(8,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED}),
                             std::make_tuple(9,  std::array<enum Move_state_enm,END_SET_DATA>{MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_INVALID  , MOVE_INVALID  , MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED, MOVE_COMPLETED})

                             ));
// clang-format on