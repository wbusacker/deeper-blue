#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <engine_functions.h>
#include <movement_const.h>
}

class T_expand_tree : public ::testing::Test {
    public:
    T_expand_tree() { }

    void SetUp() { init_engine(); }

    void TearDown() { teardown_engine(); }
};

TEST_F(T_expand_tree, expect_full_propegation) {

    /* There should be exactly 140 suggested moves */

    uint32_t unevaluated_moves  = 0;
    uint32_t unpropegated_moves = 0;
    uint32_t invalid_moves      = 0;
    uint32_t completed_moves    = 0;

    /* Count the number of individual statuses */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

        switch (move_buffer[i].state) {
            case MOVE_UNEVALUATED:
                unevaluated_moves++;
                break;
            case MOVE_UNPROPEGATED:
                unpropegated_moves++;
                break;
            case MOVE_COMPLETED:
                completed_moves++;
                break;
            case MOVE_INVALID:
                invalid_moves++;
                break;
            default:
                break;
        }
    }

    EXPECT_EQ(unevaluated_moves, 140);
    EXPECT_EQ(unpropegated_moves, 0);
    EXPECT_EQ(completed_moves, 1);
    EXPECT_EQ(invalid_moves, MOVE_BUFFER_SIZE - (140 + 1));
}

TEST_F(T_expand_tree, correct_player_set) {

    /* Count the number of individual statuses */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

        if (move_buffer[i].state == MOVE_UNEVALUATED) {
            move_buffer[i].player = PLAYER_WHITE;
        }
    }
}

TEST_F(T_expand_tree, correct_parent_set) {

    /* Count the number of individual statuses */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

        if (move_buffer[i].state == MOVE_UNEVALUATED) {
            move_buffer[i].parent_id = 0;
        }
    }
}

TEST_F(T_expand_tree, correct_piece_moved) {

    /* Count the number of individual statuses */
    for (uint32_t i = 0; i < MOVE_BUFFER_SIZE; i++) {

        if (move_buffer[i].state == MOVE_UNEVALUATED) {

            EXPECT_LT(move_buffer[i].piece_start_location, 16);
            EXPECT_GE(move_buffer[i].piece_start_location, 0);
        }
    }
}