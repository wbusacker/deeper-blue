#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <movement_functions.h>
}

class T_evaluate_king_move : public ::testing::TestWithParam<std::tuple<std::tuple<int, int>, enum Player_type_enm>> {
    public:
    T_evaluate_king_move() { }

    void SetUp() {

        /* Set all positions in the board to zero */
        for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
            board[i] = NO_PIECE;
        }

        source      = std::get<0>(std::get<0>(GetParam()));
        destination = std::get<1>(std::get<0>(GetParam()));

        player = std::get<1>(GetParam());

        source_piece    = (player == PLAYER_WHITE) ? WHITE_KING : BLACK_KING;
        capture_piece   = (player == PLAYER_WHITE) ? BLACK_KING : WHITE_KING;
        collision_piece = (player == PLAYER_WHITE) ? WHITE_KING : BLACK_KING;
    }

    void TearDown() { }

    int8_t source;
    int8_t destination;

    enum Player_type_enm player;

    enum Piece_types_enm board[NUM_BOARD_SPACES];

    enum Piece_types_enm source_piece;
    enum Piece_types_enm capture_piece;
    enum Piece_types_enm collision_piece;
};

TEST_P(T_evaluate_king_move, legal_open_move) {

    board[source] = source_piece;

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, source, destination));
}

TEST_P(T_evaluate_king_move, legal_capture_move) {

    board[source]      = source_piece;
    board[destination] = capture_piece;

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, source, destination));
}

TEST_P(T_evaluate_king_move, illegal_move_collision) {

    board[source]      = source_piece;
    board[destination] = collision_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, source, destination));
}

INSTANTIATE_TEST_SUITE_P(Check_king_movement,
                         T_evaluate_king_move,
                         ::testing::Combine(::testing::Values(
                                              /* Source, Destination, Collision */
                                              std::make_tuple(27, 35),
                                              std::make_tuple(27, 36),
                                              std::make_tuple(27, 28),
                                              std::make_tuple(27, 20),
                                              std::make_tuple(27, 19),
                                              std::make_tuple(27, 18),
                                              std::make_tuple(27, 26),
                                              std::make_tuple(27, 34)),
                                            ::testing::Values(PLAYER_WHITE, PLAYER_BLACK)));