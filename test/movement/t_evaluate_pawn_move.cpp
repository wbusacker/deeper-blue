#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <movement_functions.h>
}

class T_evaluate_pawn_move : public ::testing::TestWithParam<enum Player_type_enm> {
    public:
    T_evaluate_pawn_move() { }

    void SetUp() {

        /* Set all positions in the board to zero */
        for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
            board[i] = NO_PIECE;
        }

        player = GetParam();

        pawn_location = (player == PLAYER_WHITE) ? 9 : 49;
        direction     = (player == PLAYER_WHITE) ? 1 : -1;

        our_piece   = (player == PLAYER_WHITE) ? WHITE_PAWN : BLACK_PAWN;
        other_piece = (player == PLAYER_WHITE) ? BLACK_PAWN : WHITE_PAWN;

        board[pawn_location] = our_piece;
    }

    void TearDown() { }

    enum Piece_types_enm board[NUM_BOARD_SPACES];

    enum Player_type_enm player;
    int8_t               pawn_location;
    int8_t               direction;

    enum Piece_types_enm our_piece;
    enum Piece_types_enm other_piece;
};

TEST_P(T_evaluate_pawn_move, legal_forward_one) {

    /* Set destination */
    int8_t destination = pawn_location + (8 * direction);

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_forward_one) {

    /* Set destination */
    int8_t destination = pawn_location + (8 * direction);
    board[destination] = our_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, legal_forward_two) {

    /* Set destination */
    int8_t destination = pawn_location + (16 * direction);

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_forward_two_on_first) {

    /* Set destination */
    int8_t destination                     = pawn_location + (16 * direction);
    board[pawn_location + (8 * direction)] = our_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_forward_two_on_second) {

    /* Set destination */
    int8_t destination = pawn_location + (16 * direction);
    board[destination] = our_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, legal_capture_right) {

    /* Set destination */
    int8_t destination = pawn_location + (9 * direction);
    board[destination] = other_piece;

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_capture_right_own) {

    /* Set destination */
    int8_t destination = pawn_location + (9 * direction);
    board[destination] = our_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_capture_right_empy) {

    /* Set destination */
    int8_t destination = pawn_location + (9 * direction);

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, legal_capture_left) {

    /* Set destination */
    int8_t destination = pawn_location + (7 * direction);
    board[destination] = other_piece;

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_capture_left_own) {

    /* Set destination */
    int8_t destination = pawn_location + (7 * direction);
    board[destination] = our_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_capture_left_empy) {

    /* Set destination */
    int8_t destination = pawn_location + (7 * direction);

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

TEST_P(T_evaluate_pawn_move, illegal_move_out_of_range) {

    /* Set destination */
    int8_t destination = pawn_location + 1;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, pawn_location, destination));
}

INSTANTIATE_TEST_SUITE_P(Check_pawn_movement, T_evaluate_pawn_move, ::testing::Values(PLAYER_WHITE, PLAYER_BLACK));