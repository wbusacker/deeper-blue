#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <movement_functions.h>
}

class T_evaluate_queen_move :
    public ::testing::TestWithParam<std::tuple<std::tuple<int, int, int>, enum Player_type_enm>> {
    public:
    T_evaluate_queen_move() { }

    void SetUp() {

        /* Set all positions in the board to zero */
        for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
            board[i] = NO_PIECE;
        }

        source      = std::get<0>(std::get<0>(GetParam()));
        destination = std::get<1>(std::get<0>(GetParam()));
        collision   = std::get<2>(std::get<0>(GetParam()));

        player = std::get<1>(GetParam());

        source_piece    = (player == PLAYER_WHITE) ? WHITE_QUEEN : BLACK_QUEEN;
        capture_piece   = (player == PLAYER_WHITE) ? BLACK_QUEEN : WHITE_QUEEN;
        collision_piece = (player == PLAYER_WHITE) ? WHITE_QUEEN : BLACK_QUEEN;
    }

    void TearDown() { }

    int8_t source;
    int8_t destination;
    int8_t collision;

    enum Player_type_enm player;

    enum Piece_types_enm board[NUM_BOARD_SPACES];

    enum Piece_types_enm source_piece;
    enum Piece_types_enm capture_piece;
    enum Piece_types_enm collision_piece;
};

TEST_P(T_evaluate_queen_move, legal_open_move) {

    board[source] = source_piece;

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, source, destination));
}

TEST_P(T_evaluate_queen_move, legal_capture_move) {

    board[source]      = source_piece;
    board[destination] = capture_piece;

    EXPECT_EQ(LEGAL_MOVE, evaluate_move_attempt(board, source, destination));
}

TEST_P(T_evaluate_queen_move, illegal_move_collision) {

    board[source]      = source_piece;
    board[destination] = capture_piece;
    board[collision]   = collision_piece;

    EXPECT_EQ(ILLEGAL_MOVE, evaluate_move_attempt(board, source, destination));
}

INSTANTIATE_TEST_SUITE_P(Check_queen_movement,
                         T_evaluate_queen_move,
                         ::testing::Combine(::testing::Values(
                                              /* Source, Destination, Collision */
                                              std::make_tuple(24, 31, 28), /* Horizontal Right Movement */
                                              std::make_tuple(31, 24, 28), /* Horizontal Left  Movement */
                                              std::make_tuple(2, 58, 26),  /* Vertical   Up    Movement */
                                              std::make_tuple(58, 2, 26),  /* Vertical   Down  Movement */
                                              std::make_tuple(0, 63, 27),  /* Diag Right Up    Movement */
                                              std::make_tuple(63, 0, 27),  /* Diag Right Down  Movement */
                                              std::make_tuple(7, 56, 28),  /* Diag Left  Up    Movement */
                                              std::make_tuple(56, 7, 28)   /* Diag Left  Down  Movement */
                                              ),
                                            ::testing::Values(PLAYER_WHITE, PLAYER_BLACK)));