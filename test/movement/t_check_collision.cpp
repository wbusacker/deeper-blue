#include <gtest/gtest.h>
#include <tuple>

extern "C" {
#include <movement_functions.h>
}

class T_check_collision : public ::testing::TestWithParam<std::tuple<int, int>> {
    public:
    T_check_collision() { }

    void SetUp() {

        /* Set all positions in the board to zero */
        for (uint8_t i = 0; i < NUM_BOARD_SPACES; i++) {
            board[i] = NO_PIECE;
        }
    }

    void TearDown() { }

    enum Piece_types_enm board[NUM_BOARD_SPACES];
    const uint8_t        starting_position = 27;
};

TEST_P(T_check_collision, expect_collision) {

    /* Use the first coordinate as the piece we expect to collide against */
    board[std::get<0>(GetParam())] = WHITE_PAWN;

    EXPECT_TRUE(check_collision(board, starting_position, std::get<1>(GetParam())));
}

TEST_P(T_check_collision, expect_no_collision) {

    /* Use the first coordinate as the piece we expect to collide against */
    board[std::get<1>(GetParam())] = WHITE_PAWN;

    EXPECT_FALSE(check_collision(board, starting_position, std::get<0>(GetParam())));
}

INSTANTIATE_TEST_SUITE_P(Check_collision,
                         T_check_collision,
                         ::testing::Values(std::make_tuple(28, 30),
                                           std::make_tuple(36, 63),
                                           std::make_tuple(35, 59),
                                           std::make_tuple(34, 48),
                                           std::make_tuple(26, 24),
                                           std::make_tuple(18, 0),
                                           std::make_tuple(19, 3),
                                           std::make_tuple(20, 6)));