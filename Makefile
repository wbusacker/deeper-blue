# Copyright (c) 2022 Will Busacker
# See project license for more details
#
# Project:  Base C Project
# Filename: Makefile
# Purpose:  Automatically collect all files within the individual CSCs and 
#      compile into a set of optimized production and unoptimized debug
#           main program and unit test program

###############################################################################
#                                                                             #
#                   Primary Project Configuration Settings                    #
#                                                                             #
###############################################################################

USER_SET_PRODUCTION_COMPILE_FLAGS := -fshort-enums
USER_SET_PRODUCTION_LINKER_FLAGS  := -lpthread

USER_SET_DEBUG_COMPILE_FLAGS      := -fshort-enums
USER_SET_DEBUG_LINKER_FLAGS       :=

# Leave blank for system default, if specificed place ending /
USER_SET_GCC_DIRECTORY            := /opt/gcc/bin

# Leave blank for same level as Makefile
USER_SET_EXECUTABLE_OUTPUT_DIR    := 

###############################################################################
#                                                                             #
#                          Project Directory Settings                         #
#                                                                             #
###############################################################################

PROG_SRC := ./src
TEST_SRC := ./test

FINAL_OUTPUT_DIR        := .
BUILD_PRODUCTS_DIR      := $(FINAL_OUTPUT_DIR)/bld

DEPENDENCY_DIR          := $(BUILD_PRODUCTS_DIR)/deps

DEBUG_OBJECT_DIR        := $(BUILD_PRODUCTS_DIR)/debug_obj
PRODUCTION_OBJECT_DIR   := $(BUILD_PRODUCTS_DIR)/production_obj

###############################################################################
#                                                                             #
#                            Source File Discovery                            #
#                                                                             #
###############################################################################

# Find all regular program source files needed to be compiled
PROG_SOURCE_FILES                := $(sort $(shell find $(PROG_SRC) -type f -name *.c -o -name *.cpp))
PROG_OBJECT_FILES                := $(patsubst %.c, %.o, $(patsubst %.cpp, %.o, $(notdir $(PROG_SOURCE_FILES))))
PROG_PRODUCTION_DEPENDENCY_FILES := $(addprefix $(DEPENDENCY_DIR)/, $(patsubst %.c, %.p.d, $(patsubst %.cpp, %.p.d, $(notdir $(PROG_SOURCE_FILES)))))
PROG_DEBUG_DEPENDENCY_FILES      := $(addprefix $(DEPENDENCY_DIR)/, $(patsubst %.c, %.d.d, $(patsubst %.cpp, %.d.d, $(notdir $(PROG_SOURCE_FILES)))))
PROG_SOURCE_DIRECTORIES          := $(sort $(shell find $(PROG_SRC) -maxdepth 1 -mindepth 1 -type d))
PROG_INCLUDE_DIRECTORIES         := $(addprefix -I, $(PROG_SOURCE_DIRECTORIES))

# Find all program test source files needed to be compiled
TEST_SOURCE_FILES                := $(sort $(shell find $(TEST_SRC) -type f -name *.c -o -name *.cpp))
TEST_OBJECT_FILES                := $(patsubst %.c, %.t.o, $(patsubst %.cpp, %.t.o, $(notdir $(TEST_SOURCE_FILES))))
TEST_PRODUCTION_DEPENDENCY_FILES := $(addprefix $(DEPENDENCY_DIR)/, $(patsubst %.c, %.p.t.d, $(patsubst %.cpp, %.p.t.d, $(notdir $(TEST_SOURCE_FILES)))))
TEST_DEBUG_DEPENDENCY_FILES      := $(addprefix $(DEPENDENCY_DIR)/, $(patsubst %.c, %.d.t.d, $(patsubst %.cpp, %.d.t.d, $(notdir $(TEST_SOURCE_FILES)))))
TEST_SOURCE_DIRECTORIES          := $(sort $(shell find $(TEST_SRC) -maxdepth 1 -mindepth 1 -type d))
TEST_INCLUDE_DIRECTORIES         := $(addprefix -I, $(TEST_SOURCE_DIRECTORIES))

# Build up the list of both versions of the objects for the main program
PROG_PRODUCTION_OBJECTS := $(addprefix $(PRODUCTION_OBJECT_DIR)/, $(PROG_OBJECT_FILES))
PROG_DEBUG_OBJECTS      := $(addprefix $(DEBUG_OBJECT_DIR)/, $(PROG_OBJECT_FILES))

# Build up the list of both versions of the objects for the test program
TEST_PRODUCTION_OBJECTS := $(addprefix $(PRODUCTION_OBJECT_DIR)/, $(TEST_OBJECT_FILES)) $(filter-out %main.o, $(PROG_PRODUCTION_OBJECTS))
TEST_DEBUG_OBJECTS      := $(addprefix $(DEBUG_OBJECT_DIR)/, $(TEST_OBJECT_FILES)) $(filter-out %main.o, $(PROG_DEBUG_OBJECTS))

# Build list of all file dependencies
DEPENDENCY_FILES        := $(PROG_PRODUCTION_DEPENDENCY_FILES) $(PROG_DEBUG_DEPENDENCY_FILES) $(TEST_PRODUCTION_DEPENDENCY_FILES) $(TEST_DEBUG_DEPENDENCY_FILES)

# Find all directories within the project to look for headers
INCLUDE_DIRECTORIES     := $(PROG_INCLUDE_DIRECTORIES) $(TEST_INCLUDE_DIRECTORIES)

###############################################################################
#                                                                             #
#                               Build Products                                #
#                                                                             #
###############################################################################

PROJECT_NAME         := $(shell basename $(CURDIR))

PROG_PRODUCTION_NAME     := $(PROJECT_NAME)
PROG_PRODUCTION_EXE      := $(PROG_PRODUCTION_NAME)
PROG_PRODUCTION_PRODUCTS := $(PROG_PRODUCTION_EXE) $(PROG_PRODUCTION_OBJECTS)

PROG_DEBUG_NAME          := $(PROJECT_NAME).dbg
PROG_DEBUG_EXE           := $(PROG_DEBUG_NAME)
PROG_DEBUG_ASM           := $(BUILD_PRODUCTS_DIR)/$(PROG_DEBUG_NAME).asm
PROG_DEBUG_SYM           := $(BUILD_PRODUCTS_DIR)/$(PROG_DEBUG_NAME).sym
PROG_DEBUG_PRODUCTS      := $(PROG_DEBUG_EXE) $(PROG_DEBUG_OBJECTS)

TEST_PRODUCTION_NAME     := $(PROJECT_NAME).test
TEST_PRODUCTION_EXE      := $(TEST_PRODUCTION_NAME)
TEST_PRODUCTION_PRODUCTS := $(TEST_PRODUCTION_EXE) $(TEST_PRODUCTION_OBJECTS)

TEST_DEBUG_NAME          := $(PROJECT_NAME).test.dbg
TEST_DEBUG_EXE           := $(TEST_DEBUG_NAME)
TEST_DEBUG_ASM           := $(BUILD_PRODUCTS_DIR)/$(TEST_DEBUG_NAME).asm
TEST_DEBUG_SYM           := $(BUILD_PRODUCTS_DIR)/$(TEST_DEBUG_NAME).sym
TEST_DEBUG_PRODUCTS      := $(TEST_DEBUG_EXE) $(TEST_DEBUG_OBJECTS)

ALL_BUILD_PRODUCTS := $(PROG_PRODUCTION_PRODUCTS) $(PROG_DEBUG_PRODUCTS) $(TEST_PRODUCTION_PRODUCTS) $(TEST_DEBUG_PRODUCTS) $(PROG_DEBUG_ASM) $(PROG_DEBUG_SYM) $(TEST_DEBUG_ASM) $(TEST_DEBUG_SYM)

###############################################################################
#                                                                             #
#                            Make Control Settings                            #
#                                                                             #
###############################################################################

# Command Mute Flag
M     := @
VPATH  = $(PROG_SOURCE_DIRECTORIES) $(TEST_SOURCE_DIRECTORIES)

###############################################################################
#                                                                             #
#                               Build Settings                                #
#                                                                             #
###############################################################################

CC      := $(USER_SET_GCC_DIRECTORY)/gcc
CXX     := $(USER_SET_GCC_DIRECTORY)/g++
OBJDUMP := objdump
NM      := nm

C_WARN   := -Wall -Wextra -Werror
CXX_WARN := $(C_WARN) -Wpedantic

PROG_LINK_LIBRARIES := $(USER_SET_PRODUCTION_LINKER_FLAGS)
TEST_LINK_LIBRARIES := $(PROG_LINK_LIBRARIES) -lgtest -lgmock -lpthread
DEBUG_LINK_LIBRARIES := $(USER_SET_DEBUG_LINKER_FLAGS) -lgcov

DEBUG_COMPILE_FLAGS      := $(USER_SET_DEBUG_COMPILE_FLAGS) -O0 -g -fprofile-arcs -ftest-coverage -fPIC -ffile-prefix-map=${CURDIR}=.
PRODUCTION_COMPILE_FLAGS := $(USER_SET_PRODUCTION_COMPILE_FLAGS) -O3 -flto

DISASSEMBLY_FLAGS := -d -l
SYMBOL_FLAGS      := -C -n 

DEPENDENCY_FLAGS = -MT $@ -MMD -MP -MF $(word 2, $^)

###############################################################################
#                                                                             #
#                              Colored Messaging                              #
#                                                                             #
###############################################################################

COLOR_RESET  := \033[0m
COLOR_BLUE   := \033[0;34m
COLOR_RED    := \033[0;31m
COLOR_PURPLE := \033[0;35m
COLOR_GREEN  := \033[0;32m
COLOR_YELLOW := \033[0;33m

DEBUG_COMPILE_MESSAGE      = $(M)env printf "${COLOR_BLUE}Compiling ${COLOR_YELLOW}Debug     ${COLOR_RESET} %-80s \n" $1
PRODUCTION_COMPILE_MESSAGE = $(M)env printf "${COLOR_BLUE}Compiling ${COLOR_GREEN}Production${COLOR_RESET} %-80s \n" $1
LINK_MESSAGE               = $(M)env printf "${COLOR_RED}Linking${COLOR_RESET}    %s \n" $1
DEBUG_MESSAGE              = $(M)env printf "${COLOR_PURPLE}Generating${COLOR_RESET} %s \n" $1

###############################################################################
#                                                                             #
#                                 User Rules                                  #
#                                                                             #
###############################################################################

default: production_program debug_program production_test debug_test

all: default debug_program_symbols debug_test_symbols debug_program_assembly debug_test_assembly

production_program: $(PROG_PRODUCTION_EXE)
debug_program: $(PROG_DEBUG_EXE)

production_test: $(TEST_PRODUCTION_EXE)
debug_test: $(TEST_DEBUG_EXE)

debug_program_assembly: $(PROG_DEBUG_ASM)
debug_test_assembly: $(TEST_DEBUG_ASM)

debug_program_symbols: $(PROG_DEBUG_SYM)
debug_test_symbols: $(TEST_DEBUG_SYM)

clean:
	$(M) rm -rf $(ALL_BUILD_PRODUCTS)

cleanall: clean
	$(M) rm -rf $(BUILD_PRODUCTS_DIR)

test: $(TEST_PRODUCTION_EXE)
	./$(TEST_PRODUCTION_EXE)

test_debug: $(TEST_DEBUG_EXE)
	./$(TEST_DEBUG_EXE)

coverage: $(TEST_DEBUG_EXE)
	./$(TEST_DEBUG_EXE)
	gcovr -r . --html-details -o $(BUILD_PRODUCTS_DIR)/report.html

format:
	$(M)clang-format -i ./$(PROG_SRC)/**/* ./$(TEST_SRC)/**/*

###############################################################################
#                                                                             #
#                                 Meta Rules                                  #
#                                                                             #
###############################################################################

info:
	$(M)echo "Directory for Program Source Code  " $(PROG_SRC)
	$(M)echo "Directory for Test Source Code     " $(TEST_SRC)
	$(M)echo "Directory for Final Build Products " $(FINAL_OUTPUT_DIR)
	$(M)echo "Directory for Source Dependencies  " $(DEPENDENCY_DIR)
	$(M)echo "Directory for Debug Objects        " $(DEBUG_OBJECT_DIR)
	$(M)echo "Directory for Production Objects   " $(PRODUCTION_OBJECT_DIR)
	$(M)echo "Directory for Building Includes    " $(INCLUDE_DIRECTORIES)
	$(M)echo "VPATH                              " $(VPATH)
	$(M)echo
	$(M)echo Known Program Source Files
	$(M)echo $(PROG_SOURCE_FILES)
	$(M)echo
	$(M)echo Known Test Source Files
	$(M)echo $(TEST_SOURCE_FILES)
	$(M)echo
	$(M)echo Known Production Program Files
	$(M)echo $(PROG_PRODUCTION_OBJECTS)
	$(M)echo
	$(M)echo Known Production Test Files
	$(M)echo $(TEST_PRODUCTION_OBJECTS)
	$(M)echo
	$(M)echo Known Debug Program Files
	$(M)echo $(PROG_DEBUG_OBJECTS)
	$(M)echo
	$(M)echo Known Debug Test Files
	$(M)echo $(TEST_DEBUG_OBJECTS)
	$(M)echo

.PHONY: default all production_program debug_program production_test debug_test             \
		debug_program_assembly debug_test_assembly debug_program_symbols debug_test_symbols \
		clean cleanall test test_debug coverage format\

###############################################################################
#                                                                             #
#                                 Build Rules                                 #
#                                                                             #
###############################################################################

# Compilation Rules
$(PRODUCTION_OBJECT_DIR)/%.o: %.c $(DEPENDENCY_DIR)/%.p.d | $(DEPENDENCY_DIR) $(PRODUCTION_OBJECT_DIR)
	$(call PRODUCTION_COMPILE_MESSAGE, $<)
	$(M) $(CC) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(C_WARN) $(PRODUCTION_COMPILE_FLAGS) -o $@ -c $<
	
$(DEBUG_OBJECT_DIR)/%.o: %.c $(DEPENDENCY_DIR)/%.d.d | $(DEPENDENCY_DIR) $(DEBUG_OBJECT_DIR)
	$(call DEBUG_COMPILE_MESSAGE, $<)
	$(M) $(CC) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(C_WARN) $(DEBUG_COMPILE_FLAGS) -o $@ -c $<

$(PRODUCTION_OBJECT_DIR)/%.o: %.cpp $(DEPENDENCY_DIR)/%.p.d | $(DEPENDENCY_DIR) $(PRODUCTION_OBJECT_DIR)
	$(call PRODUCTION_COMPILE_MESSAGE, $<)
	$(M) $(CXX) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(CXX_WARN) $(PRODUCTION_COMPILE_FLAGS) -o $@ -c $<
	
$(DEBUG_OBJECT_DIR)/%.o: %.cpp $(DEPENDENCY_DIR)/%.d.d | $(DEPENDENCY_DIR) $(DEBUG_OBJECT_DIR)
	$(call DEBUG_COMPILE_MESSAGE, $<)
	$(M) $(CXX) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(CXX_WARN) $(DEBUG_COMPILE_FLAGS) -o $@ -c $<

$(PRODUCTION_OBJECT_DIR)/%.t.o: %.c $(DEPENDENCY_DIR)/%.p.t.d | $(DEPENDENCY_DIR) $(PRODUCTION_OBJECT_DIR)
	$(call PRODUCTION_COMPILE_MESSAGE, $<)
	$(M) $(CC) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(C_WARN) $(PRODUCTION_COMPILE_FLAGS) -o $@ -c $<
	
$(DEBUG_OBJECT_DIR)/%.t.o: %.c $(DEPENDENCY_DIR)/%.d.t.d | $(DEPENDENCY_DIR) $(DEBUG_OBJECT_DIR)
	$(call DEBUG_COMPILE_MESSAGE, $<)
	$(M) $(CC) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(C_WARN) $(DEBUG_COMPILE_FLAGS) -o $@ -c $<

$(PRODUCTION_OBJECT_DIR)/%.t.o: %.cpp $(DEPENDENCY_DIR)/%.p.t.d | $(DEPENDENCY_DIR) $(PRODUCTION_OBJECT_DIR)
	$(call PRODUCTION_COMPILE_MESSAGE, $<)
	$(M) $(CXX) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(CXX_WARN) $(PRODUCTION_COMPILE_FLAGS) -o $@ -c $<
	
$(DEBUG_OBJECT_DIR)/%.t.o: %.cpp $(DEPENDENCY_DIR)/%.d.t.d | $(DEPENDENCY_DIR) $(DEBUG_OBJECT_DIR)
	$(call DEBUG_COMPILE_MESSAGE, $<)
	$(M) $(CXX) $(DEPENDENCY_FLAGS) $(INCLUDE_DIRECTORIES) $(CXX_WARN) $(DEBUG_COMPILE_FLAGS) -o $@ -c $<

# Linker Rules
$(PROG_PRODUCTION_EXE): $(PROG_PRODUCTION_OBJECTS)
	$(call LINK_MESSAGE, $@)
	$(M) $(CXX) $(LINK_FLAGS) -o $@ $^ $(PROG_LINK_LIBRARIES)

$(PROG_DEBUG_EXE): $(PROG_DEBUG_OBJECTS)
	$(call LINK_MESSAGE, $@)
	$(M) $(CXX) $(LINK_FLAGS) -o $@ $^ $(PROG_LINK_LIBRARIES) $(DEBUG_LINK_LIBRARIES)

$(TEST_PRODUCTION_EXE): $(TEST_PRODUCTION_OBJECTS)
	$(call LINK_MESSAGE, $@)
	$(M) $(CXX) $(LINK_FLAGS) -o $@ $^ $(TEST_LINK_LIBRARIES)

$(TEST_DEBUG_EXE): $(TEST_DEBUG_OBJECTS)
	$(call LINK_MESSAGE, $@)
	$(M) $(CXX) $(LINK_FLAGS) -o $@ $^ $(TEST_LINK_LIBRARIES) $(DEBUG_LINK_LIBRARIES)

# Debug Output Rules
$(BUILD_PRODUCTS_DIR)/%.asm: %
	$(call DEBUG_MESSAGE, $@)
	$(M) $(OBJDUMP) $(DISASSEMBLY_FLAGS) $^ > $@

$(BUILD_PRODUCTS_DIR)/%.sym: %
	$(call DEBUG_MESSAGE, $@)
	$(M) $(NM) $(SYMBOL_FLAGS) $^ > $@

# Directory Rules
$(DEPENDENCY_DIR):
	$(M)mkdir -p $@

$(DEBUG_OBJECT_DIR):
	$(M)mkdir -p $@

$(PRODUCTION_OBJECT_DIR):
	$(M)mkdir -p $@

# Build System Requirements
$(DEPENDENCY_FILES):

include $(wildcard $(DEPENDENCY_FILES))